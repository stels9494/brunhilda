<?php



/*просмотр сайтов клиента через поддомен*/
$domain = 'localhost';
Route::domain('{prefix}.'.$domain)->group(function () {
    $slug = '';
    $levelInclude = 5;
    for ($i = 1; $i <= $levelInclude; $i++)
        $slug .= '{slug'.$i.'?}/';
    // return Route::get($slug ,'Admin\OrderController@fake');

    Route::get($slug, function ($prefix){
        //найти страницу
        dd($prefix);
    });
});

/*просмотр сайта клиента через их домен*/
/*
Порядок действий:
    1) Определить prefix сайта и проверить на существование и активность в бд
    2) Определить какие slug заданы
*/
Route::get('/', 'MainController@index')->name('main.index');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Route::resource('/construct-panel', 'Admin\Brunhilda\MainController');

Route::group(['prefix' => '/admin', 'middleware' => ['auth']], function (){
    Route::resource('/', 'Admin\Brunhilda\DashboardController');
    Route::group(['middleware' => ['can.show.tpl', 'is.admin']], function (){
        Route::resource('/tpl', 'Admin\Brunhilda\TplController');
        Route::resource('/tplType', 'Admin\Brunhilda\TplTypeController');
        Route::resource('/jsLib', 'Admin\Brunhilda\JsLibController');
        Route::resource('/cssLib', 'Admin\Brunhilda\CssLibController');
        Route::resource('/field', 'Admin\Brunhilda\FieldController');
    });
    Route::group(['middleware' => ['is.admin']], function (){
        Route::resource('/sites', 'Admin\Brunhilda\SiteController');
        Route::resource('/sites/{site}/pages', 'Admin\Brunhilda\PageController');
        Route::resource('/users', 'Admin\UserController');
        Route::resource('/roles', 'Admin\RoleController');
        Route::resource('/accounts', 'Admin\AccountController');
    });
});

Route::get('/construct/{any?}', 'Admin\Brunhilda\SpaController@index')->where('any', '.*')->name('construct')->middleware(['auth' /*'can.show.visual.construct',*/]);







/*можно потом удалить*/

// Route::get('fake', 'Admin\OrderController@fake')->name('fake');


//если есть роут в таблице pages, то включить его
// \App\Services\UserRoutesService::routes();


