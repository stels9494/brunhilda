<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
адресация для конструктора:
    - запрос всех шаблонов
    - запрос шаблонов по типу
    - запрос определенного шаблона
*/
Route::group(['prefix' => 'brunhilda', 'middleware' => ['jwt.auth']], function () {
    //список шаблонов - не используется
    // Route::get('/templates', 'Api\Brunhilda\TplController@templates');

    //список сайтов
    Route::get('/sites', 'Api\Brunhilda\SiteController@sites');//под конкретного пользователя
    //список страниц сайта
    Route::get('/sites/{site_id}/pages', 'Api\Brunhilda\PageController@pages');//под конкретного пользователя
    //создать страницу
    Route::get('/sites/{site_id}/pages/create', 'Api\Brunhilda\PageController@createPage');//под конкретного пользователя
    //Вывод все шаблонов и блоков текущей страницы
    Route::get('/sites/{site_id}/pages/{page_id}', 'Api\Brunhilda\PageController@blocksAndTpls');//под конкретного пользователя
    //блоки страницы - не используется
    Route::get('/sites/{site_id}/pages/{page_id}/blocks', 'Api\Brunhilda\BlockController@blocks');//под контролем конкретного пользователя
    //создать блок на странице
    Route::get('/sites/{site_id}/pages/{page_id}/blocks/create', 'Api\Brunhilda\BlockController@createBlock');//под конкретного пользователя


    //меняет два блока местами
    Route::patch('/sites/{site_id}/pages/{page_id}/blocks/move', 'Api\Brunhilda\BlockController@moveBlocks');//под конкретного пользователя


    //удаление всех блоков - временный для разработки
    Route::get('/pages/{page_id}/delete', 'Api\Brunhilda\BlockController@deleteAllBlocks'); //временный




    //копирование блока
    // Route::get('/sites/{site_id}/pages/{page_id}/blocks/{block_id}/duplicate', 'Api\Brunhilda\BlockController@duplicateBlock');

    //удаление блока на странице
    Route::delete('/sites/{site_id}/pages/{page_id}/blocks/{block_id}', 'Api\Brunhilda\BlockController@deleteBlock');//под конкретного пользователя

 /**********************************НОВЫЙ ТИП РОУТОВ************************************************/

    Route::resource('/pages', 'Api\Brunhilda\PageController');
    Route::resource('/dataContent', 'Api\Brunhilda\BlockController');
    Route::resource('/media', 'Api\Brunhilda\DataController');








//              Route::get('test', 'Api\Brunhilda\TplController@test1')->name('api.test');

//     Route::group(['prefix' => 'create'], function () {
//         Route::post('tpl', 'Api\Brunhilda\TplController@createTpl')->name('api.create.tpl');
//         Route::get('tplType', 'Api\Brunhilda\TplController@createTplType')->name('api.create.tplType');
//         Route::get('jsLib', 'Api\Brunhilda\TplController@createJsLib')->name('api.create.jsLib');
//         Route::get('field', 'Api\Brunhilda\TplController@createField')->name('api.create.field');
//     });

//     // Route::groupe(['prefix' => 'update'], function () {

//     // });

//     // Route::groupe(['prefix' => 'destroy'], function () {

//     // });

});

// Route::get('/users/search', 'Api\UserController@search')->name('api.users.search');


/*
|--------------------------------------------------------------------------
| JWT Routes
|--------------------------------------------------------------------------
|
*/
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});