import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
	plugins: [createPersistedState()],
  state: {
		siteId: undefined,
		token: undefined,
    permissions: {},
  },
  mutations: {
    setPermissions(state, permissions) {
			state.permissions = {};
			
			permissions.forEach(permission => {
				state.permissions[permission] = true;
			});
		},
		setSiteId(state, data) {
			state.siteId = data;
		},
		setToken(state, token) {
			state.token = token;
		}
  }
})
