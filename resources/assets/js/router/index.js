import Vue from 'vue'
import Router from 'vue-router'

import App from '../App'
import Sites from '../components/Sites'
import Site from '../components/Site'
import Workbench from '../components/Workbench'
import NotFound from '../components/NotFound'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    redirect: '/sites' 
  },{
    path: '/sites',
    component: Sites,
  },{
    path: '/site/:id',
    component: Site,
  }, {
    path: '/page/:id',
    component: Workbench
  },{ 
    path: '/404',
    component:NotFound
  },{ 
    path: '*',
    redirect: "/404" 
  }]
})
