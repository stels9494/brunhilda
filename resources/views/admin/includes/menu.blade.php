<div class="navbar nav_title" style="border: 0;">
    <a href="#" class="site_title"><i class="fa fa-paw"></i> <span>Brunhilda panel</span></a>
</div>

<div class="clearfix"></div>

<!-- menu profile quick info -->
<div class="profile clearfix">
    <div class="profile_pic">
{{--         <a href="{{ route('users.show', Auth::id()) }}">
            <img src=
                @php
                    if (isset(Auth::user()->getMedia('avatar')[0]))
                    {
                        echo URL::to('/').Auth::user()->getMedia('avatar')[0]->getUrl('small');
                    }else{
                        echo '/images/avatarByDefault.jpg';
                    }
                @endphp
             alt="..." class="img-circle profile_img">
        </a> --}}
    </div>
    <div class="profile_info">
        <span>Здравствуйте,</span>
        {{-- <h2>{{ Illuminate\Support\Facades\Auth::user()->name }}</h2> --}}
    </div>
    <div class="clearfix"></div>
</div>
<!-- /menu profile quick info -->

<br />

<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Основное</h3>
        <ul class="nav side-menu">
            @hasanyrole('admin|tpl-developer')
                <li><a><i class="fa fa-shopping-cart"></i>Конструктор<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('tpl.index') }}">Шаблоны</a></li>
                        <li><a href="{{ route('tplType.index') }}">Типы шаблонов</a></li>
                        <li><a href="{{ route('jsLib.index') }}">JS-модули</a></li>
                        <li><a href="{{ route('cssLib.index') }}">CSS-модули</a></li>
                        @role('admin')
                            <li><a href="{{ route('sites.index') }}">Сайты</a></li>
                        @endrole
                    </ul>
                </li>
            @endhasanyrole

            @role('admin')
                <li><a><i class="fa fa-shopping-cart"></i>Управление пользователями<span class="fa fa-chevron-down"></span>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('users.index') }}">Пользователи</a></li>
                        <li><a href="{{ route('accounts.index') }}">Аккаунты</a></li>
                        <li><a href="{{ route('roles.index') }}">Роли и права</a></li>
                    </ul>
                </li>
            @endrole

            @hasanyrole('admin|tpl-developer|user')
                <li><a href="{{ route('construct') }}"><i class="fa fa-home"></i>Визуальный конструктор</a></li>
            @endhasanyrole
        </ul>
    </div>


</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('logout') }}">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
</div>
<!-- /menu footer buttons -->