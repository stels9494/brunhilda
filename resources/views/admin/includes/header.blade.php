<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
    @stack('stylesheets')
    <!-- Custom Theme Style -->
    <link href="{{ "/css/admin.css" }}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            @include('admin.includes.menu')
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
{{--                     <img src=
                      @php
                          if (isset(Auth::user()->getMedia('avatar')[0]))
                          {
                              echo URL::to('/').Auth::user()->getMedia('avatar')[0]->getUrl('small');
                          }else{
                              echo '/images/avatarByDefault.jpg';
                          }
                      @endphp
                    alt="">{{ Illuminate\Support\Facades\Auth::user()->name }} --}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    {{-- <li><a href="{{ route('users.show', Auth::user()) }}"> Профиль</a></li> --}}
                    {{-- <li><a href="{{ route('personal.index') }}"> Публичный профиль</a></li> --}}
                    <li>
                      <a href="/logout" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i>
                        Log Out
                      </a> <form id="logout-form" action="/logout" method="POST" style="display: none;">{{ csrf_field() }}</form></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->