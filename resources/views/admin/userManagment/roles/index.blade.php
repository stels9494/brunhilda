@extends('admin.layouts.app')

@section('content')
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-bars"></i> Управление Ролями и Правами </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Роли</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Права</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <form action="" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="name" value="role">
                                    <div class="row">
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <input class="form-control search" type="text" placeholder="Название роли" name="role">
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <button class="btn btn-success form-control" type="submit">Добавить</button>
                                        </div>
                                    </div>
                                </form>
                                <h3 class="page-header">Список ролей:</h3>
                            </div>
                            @foreach ($roles as $role)
                                <div>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5 col-xs-12">
                                            <h4><ul>{{ $role->name }}</ul></h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <form action="{{ route('roles.store') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="name" value="permission">
                                    <div class="row">
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <input class="form-control search" type="text" placeholder="Название права" name="permission">
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <button class="btn btn-success form-control" type="submit">Добавить</button>
                                        </div>
                                    </div>
                                </form>
                                <h3 class="page-header">Список прав:</h3>
                            </div>

                            <div class="row">
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <ul>
                                        @foreach ($permissions as $permission)
                                            <li><h4>{{ $permission->name }}</h4></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

@endsection
