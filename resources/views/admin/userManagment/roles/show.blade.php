@extends('admin.layouts.app')

@push('stylesheets')
    <link rel="stylesheet" href="/vendors/multi-select/css/multi-select.css">
@endpush

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.permissions').multiSelect({
              selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Список всех прав'>",
              selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Права текщей роли'>",
              afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                  if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                  }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                  if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                  }
                });
              },
              afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
              },
              afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
              }
            });
        });
    </script>

    <script src="/vendors/multi-select/js/jquery.quicksearch.js"></script>
    <script src="/vendors/multi-select/js/jquery.multi-select.js"></script>
@endpush

@section('content')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-header">Настрока роли "{{ $role->name }}":</h3>
        </div>
        <form action="{{ route('roles.update', $role) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="item form-group">
                    <select class="permissions" id='custom-headers' multiple='multiple' name='name[]'>
                        @php
                            $arPermissions = array_pluck($role->permissions->toArray(), 'name');
                        @endphp
                        @foreach ($permissions as $permission)
                            @if (in_array($permission->name, $arPermissions))
                                <option selected value="{{ $permission->name }}">{{ $permission->name }}</option>
                            @else
                                <option value="{{ $permission->name }}">{{ $permission->name }}</option>
                            @endif

                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
                <a class="btn btn-success form-control" href="{{ route('roles.index') }}">Назад</a>
            </div>
            <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-12">
                <button class="btn btn-success form-control" type="submit">Сохранить</button>
            </div>
        </form>

      </div>
    </div>
  </div>
@endsection