@extends('admin.layouts.app')

@section('content')
<div class="x_panel">
  <div class="x_title">
    <h2>Пользователи</h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <br>
      <form action="{{ route('users.store') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input placeholder="Email нового пользователя" name="email" type="text" id="email" required="required" class="form-control">
          </div>
          <div class="col-md-5 col-sm-5 col-xs-12">
              <input placeholder="ФИО" type="text" name="name" required="required" class="form-control">
          </div>
          <div class="col-md-1 col-sm-1 col-xs-12">
              <button type="submit" class="form-control btn btn-success">Создать</button>
          </div>
        </div>
      </form>


    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Email</th>
          <th>Name</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($users as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->email }}</td>
                <td>{{ $user->name }}</td>
                <td><a href="{{ route('users.show', $user) }}"><i class="fa fa-sign-in"></i></a></td>
            </tr>
        @endforeach
      </tbody>
    </table>


  </div>
</div>
@endsection