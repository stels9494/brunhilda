@extends('admin.layouts.app')

@section('content')
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Форма Редактирвания Данных Пользователя {{-- <small>different form elements</small> --}}</h2>
                    <ul class="nav navbar-right panel_toolbox">
{{--                       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> --}}
                      <form action="{{ route('users.destroy', $user) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <button type="submit" class="btn-danger btn btn-round btn-xs"><i class="fa fa-close"></i></button>
                      </form>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form action="{{ route('users.update', $user) }}" method="post" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ФИО <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="name" type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" value="{{ $user->name }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="email" class="form-control col-md-7 col-xs-12" type="text" name="email" value="{{ $user->email }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="role" class="control-label col-md-3 col-sm-3 col-xs-12">Роль</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control col-md-7 col-xs-12" name="role">
                            @foreach ($roles as $role)
                                @if ($userRole == $role->name)
                                    <option selected value="{{ $role->name }}">{{ $role->name }}</option>
                                @else
                                    <option value="{{ $role->name }}">{{ $role->name }}</option>
                                @endif

                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="{{ route('users.show', $user) }}" class="btn btn-primary">Назад</a>
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
@endsection