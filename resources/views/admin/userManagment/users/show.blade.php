@extends('admin.layouts.app')

@section('content')
<?php
setlocale(LC_TIME, 'ru_RU.utf-8');
?>
<div class="x_panel">

                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h1>
                                          <i class="fa fa-globe"></i> Анкета пользователя.
                                          <small class="pull-right">Дата регистрации: {{ \Carbon\Carbon::parse($user->created_at)->formatLocalized('%d %B %Y') }}</small>
                                      </h1>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Информация пользователя
                          <address>
                                          <strong>{{ 'ФИО: '.$user->name }}</strong>
                                          <br>Email: {{ $user->email }}
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <address>
                            Техническая информация
                          </address>
                            <br><b>id #{{ $user->id }}</b>
                            <br>Активация: {{ ($user->sctivate)? 'активирован': 'не активирован'}}

                        </div>

                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-md-12">
                            <h2>Управляемые сайты:</h2>
                        </div>
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>id сайта</th>
                                <th>URL-адрес</th>
                                <th>Роль</th>
                                <th style="width: 59%">Права</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($user->accounts as $account)
                                  <tr>
                                    <td>{{ $account->site->id }}</td>
                                    <td>{{ $account->site->prefix }}</td>
                                    <td>{{ $account->roles()->first() }}</td>
                                    <td>
                                        Спец. права:
                                        @foreach ($account->permissions() as $permission)
                                            здесь будет вывод element
                                        @endforeach
                                        Права роли:
                                        @foreach ($account->roles()->first()->permissions() as $permission)
                                            {{-- expr --}}
                                        @endforeach
                                    </td>
                                    <td>$64.50</td>
                                  </tr>
                              @endforeach
{{--                               <tr>
                                <td>1</td>
                                <td>Call of Duty</td>
                                <td>455-981-221</td>
                                <td>El snort testosterone trophy driving gloves handsome gerry Richardson helvetica tousled street art master testosterone trophy driving gloves handsome gerry Richardson
                                </td>
                                <td>$64.50</td>
                              </tr> --}}

                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
{{--
                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                          <p class="lead">Payment Methods:</p>
                          <img src="images/visa.png" alt="Visa">
                          <img src="images/mastercard.png" alt="Mastercard">
                          <img src="images/american-express.png" alt="American Express">
                          <img src="images/paypal.png" alt="Paypal">
                          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                          </p>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                          <p class="lead">Amount Due 2/22/2014</p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Subtotal:</th>
                                  <td>$250.30</td>
                                </tr>
                                <tr>
                                  <th>Tax (9.3%)</th>
                                  <td>$10.34</td>
                                </tr>
                                <tr>
                                  <th>Shipping:</th>
                                  <td>$5.80</td>
                                </tr>
                                <tr>
                                  <th>Total:</th>
                                  <td>$265.24</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div> --}}
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i></button>

                          {{-- <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> << Назад</button> --}}
                          <a href="{{ route('users.edit', $user) }}" class="btn btn-success pull-right" style="margin-right: 5px;">{{-- <i class="fa fa-download"></i> --}} Редактировать</a>
                          <a href="{{ route('users.index') }}" class="btn btn-primary pull-right">{{-- <i class="fa fa-credit-card"></i> --}} Назад</a>
                          {{-- <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Редактировать</button> --}}
                        </div>
                      </div>
                    </section>
                  </div>

</div>
@endsection