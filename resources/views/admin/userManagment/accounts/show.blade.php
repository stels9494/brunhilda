@extends('admin.layouts.app')

@section('content')
    email: {{ $account->email }} <br>
    ФИО: {{ $account->name }} <br>
    Права пользователя:
    <ul>
        @foreach ($account->getAllPermissions() as $permission)
            <li>{{ $permission->name }}</li>
        @endforeach
    </ul>

    <a href="{{ route('accounts.index') }}" class="btn btn-primary">Назад</a>
    <a href="{{ route('accounts.edit', $account) }}" class="btn btn-success">Редактировать</a>

@endsection