@extends('admin.layouts.app')

@section('content')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
                <a href="{{ route('accounts.create') }}" class="btn btn-success">Создать +</a>
            </div>
        </div>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Email</th>
          <th>Name</th>
          <th>URL</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($accounts as $account)
            <tr>
                <th scope="row">{{ $account->id }}</th>
                <td>{{ $account->email }}</td>
                <td>{{ $account->name }}</td>
                <td>{{ $account->site->prefix }}</td>
                <td><a href="{{ route('accounts.show', $account) }}"><i class="fa fa-sign-in"></i></a></td>
            </tr>
        @endforeach
      </tbody>
    </table>

      </div>
    </div>
  </div>
@endsection