@extends('admin.layouts.app')

@push('stylesheets')
    <link rel="stylesheet" href="/vendors/multi-select/css/multi-select.css">
@endpush

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.permissions').multiSelect({
              selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Список всех прав'>",
              selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Права аккаунта'>",
              afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                  if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                  }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                  if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                  }
                });
              },
              afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
              },
              afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
              }
            });
        });
    </script>

    <script src="/vendors/multi-select/js/jquery.quicksearch.js"></script>
    <script src="/vendors/multi-select/js/jquery.multi-select.js"></script>
@endpush


@section('content')
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Форма Редактирования Аккаунта {{-- <small>different form elements</small> --}}</h2>
                    <ul class="nav navbar-right panel_toolbox">
{{--                       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> --}}
                      <form action="{{ route('accounts.destroy', $account) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <button type="submit" class="btn-danger btn btn-round btn-xs"><i class="fa fa-close"></i></button>
                      </form>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form action="{{ route('accounts.update', $account) }}" method="post" class="form-horizontal form-label-left">
                      {{ csrf_field() }}
                      {{ method_field('PATCH') }}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email пользователя<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input readonly="readonly" name="email" type="text" id="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ $account->email }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="domain">Адрес сайта<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input readonly="readonly" name="domain" type="text" id="domain" required="required" class="form-control col-md-7 col-xs-12" value="{{ $account->site->prefix }}">
                        </div>
                      </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <select class="permissions" id='custom-headers' multiple='multiple' name='permissions[]'>
{{--                                     @foreach ($permissions as $permission)
                                        <option value="{{ $permission->name }}">{{ $permission->name }}</option>
                                    @endforeach
 --}}
                                    @php
                                        $arPermissions = array_pluck($account->getAllPermissions()->toArray(), 'name');
                                    @endphp
                                    @foreach ($permissions as $permission)
                                        @if (in_array($permission->name, $arPermissions))
                                            <option selected value="{{ $permission->name }}">{{ $permission->name }}</option>
                                        @else
                                            <option value="{{ $permission->name }}">{{ $permission->name }}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>
                        </div>

                      {{-- <div class="ln_solid"></div> --}}
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="{{ route('accounts.show', $account) }}" class="btn btn-primary" type="button">Назад</a>
                          {{-- <button class="btn btn-primary" type="button">Отмена</button> --}}
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
@endsection