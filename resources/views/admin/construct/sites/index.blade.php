@extends('admin.layouts.app')

@section('content')
        <h3 class="page-header">Создание:</h3>
        <form action="{{ route('sites.store') }}" method="post">
            {{ csrf_field() }}
            <div class="col-md-5 col-sm-5 col-xs-12">
                <input class="form-control search" type="text" name="name" placeholder="Название сайта">
            </div>
            <div class="col-md-5 col-sm-5 col-xs-12">
                <input class="form-control search" type="text" name="prefix" placeholder="prefix-адрес">
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
                <button class="btn btn-success form-control" type="submit">Добавить</button>
            </div>
        </form>
        <h3 class="page-header">Список сайтов:</h3>

        @foreach ($sites as $site)
            <div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <h4><ul>{{ $site->name }}</ul></h4>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <h4><ul>{{ $site->prefix }}</ul></h4>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <h4><ul><?=($site->activate)? 'Включен': 'Отключен';  ?></ul></h4>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <a href="{{ route('sites.show', $site) }}" class="btn btn-success form-control">Настройка</a>
                </div>
            </div>
        @endforeach
@endsection