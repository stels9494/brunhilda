@extends('admin.layouts.app')


@push('scripts')
    <script src="/vendors/jQuery-Autocomplete/js/jquery.autocomplete.js"></script>
    <script>
        $(document).ready(function (){
            $('.user-search').autocomplete({
                serviceUrl: '/'
            });
        });
    </script>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-8">
            <h3  class="page-header">Управление сайтом "{{ $site->prefix }}"</h3>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-right">
            <form action="{{ route('sites.update', $site) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <input type="hidden" name="action" value="switch">
                <input type="hidden" name="status" value=<?=($site->activate)?'0':'1'?>>
                <h2>Статус: <?=($site->activate)?'Включен':'Выключен'?> <button type="submit" class="btn btn-round btn-success"><i class="fa fa-power-off" aria-hidden="true"></i></button></h2>
            </form>
        </div>
    </div>


    <div class="col-md-2 col-sm-2 col-xs-2">
        <h4>Название сайта</h4>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10">
        <input class="form-control search" type="text" placeholder="Название сайта" value="{{ $site->name }}">
    </div>





    <a class="btn btn-success" href="{{ route('pages.index', $site) }}">Страницы</a>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3  class="page-header">Аккаунты:</h3>
        </div>
        @if (!count($site->account))
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h4>Аккаунты не найдены</h4>
            </div>
        @endif
        @foreach ($site->accounts as $account)
            {{ dump($account) }}
        @endforeach
        {{--
            для создания аккаунта нужно указать пользователя и его роль.
            поискпользователя по email
            маска поиска с начала строки
        --}}
        <input class="form-control search user-search" placeholder="Поиск по email" type="text">
@endsection