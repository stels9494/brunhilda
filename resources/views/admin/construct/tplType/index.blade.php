@extends('admin.layouts.app')

@section('content')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">

        @foreach ($tplTypes as $type)
            <div>
                <div class="col-md-11 col-sm-11 col-xs-11">

                    <h4><ul>{{ $type->name }}</ul></h4>
                </div>
                <div class="col-md-1 col-sm-1 col-xs-1">

                </div>
            </div>
        @endforeach

        @can('create-tpl')
            <form action="{{ route('tplType.store') }}" method="post">
                {{ csrf_field() }}
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control search" type="text" name="name" placeholder="Название">
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                    <button class="btn btn-success form-control" type="submit">Добавить</button>
                </div>
            </form>
        @endcan


      </div>
    </div>
  </div>
@endsection