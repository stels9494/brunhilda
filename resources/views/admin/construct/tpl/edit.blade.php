@extends('admin.layouts.app')

@push('stylesheets')
    <link rel="stylesheet" href="/vendors/multi-select/css/multi-select.css">
@endpush


@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.jslibs').multiSelect({
              selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Список js-модулей'>",
              selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Подключенные js-модули'>",
              afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                  if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                  }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                  if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                  }
                });
              },
              afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
              },
              afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
              }
            });
            $('.csslibs').multiSelect({
              selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Список css-модулей'>",
              selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Подключенные css-модули'>",
              afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                  if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                  }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                  if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                  }
                });
              },
              afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
              },
              afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
              }
            });
        });
    </script>

    <script src="/vendors/multi-select/js/jquery.quicksearch.js"></script>
    <script src="/vendors/multi-select/js/jquery.multi-select.js"></script>
@endpush

@section('content')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">

          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Основное</a>
            </li>
            <li role="presentation" class="">
              <a href="#tab_content2" id="profile-tab2" role="tab" data-toggle="tab" aria-expanded="false">HTML</a>
            </li>
            <li role="presentation" class="">
              <a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">CSS</a>
            </li>
            <li role="presentation" class="">
              <a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">JS</a>
            </li>
            <li role="presentation" class="">
              <a href="#tab_content5" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">JS-модули</a>
            </li>
            <li role="presentation" class="">
              <a href="#tab_content6" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">CSS-модули</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">

            <form action="{{ route('tpl.update', $tpl) }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{ method_field('PATCH') }}



              <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <a class="btn btn-success form-control" href="{{ route('tpl.index') }}">Назад</a>
                </div>
                <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-12">
                    <button class="btn btn-success form-control" type="submit">Сохранить</button>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control search" type="text" name="name" placeholder="Название" value="{{ $tpl->name }}">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control search" name="type_id">
                        @foreach ($tplTypes as $type)
                            <option
                                @if ($type->id == $tpl->type_id)
                                     {{ 'selected' }}
                                 @endif
                                value="{{ $type->id }}">{{ $type->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input class="search" type="file">
                </div>
              </div>



              <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <a class="btn btn-success form-control" href="{{ route('tpl.index') }}">Назад</a>
                </div>
                <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-12">
                    <button class="btn btn-success form-control" type="submit">Сохранить</button>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <textarea class="form-control search" name="html" cols="30" rows="30" placeholder="HTML">{{ $tpl->html }}</textarea>
                </div>
              </div>


              <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <a class="btn btn-success form-control" href="{{ route('tpl.index') }}">Назад</a>
                </div>
                <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-12">
                    <button class="btn btn-success form-control" type="submit">Сохранить</button>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <textarea class="form-control search" name="css" cols="30" rows="30" placeholder="CSS">{{ $tpl->css }}</textarea>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <a class="btn btn-success form-control" href="{{ route('tpl.index') }}">Назад</a>
                </div>
                <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-12">
                    <button class="btn btn-success form-control" type="submit">Сохранить</button>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <textarea class="form-control search" name="js" cols="30" rows="30" placeholder="JS">{{ $tpl->js }}</textarea>
                </div>
              </div>


              <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <a class="btn btn-success form-control" href="{{ route('tpl.index') }}">Назад</a>
                </div>
                <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-12">
                    <button class="btn btn-success form-control" type="submit">Сохранить</button>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <?php
                    $js_libs = json_decode($tpl->js_libs) ?? [];
                    // dd($js_libs);
                  ?>
                    <div class="item form-group">
                        <select class="jslibs" id='custom-headers' multiple='multiple' name='js_libs[]'>
                            @foreach ($jsLibs ?? [] as $jsLib)
                                <option <?php if (in_array($jsLib->url, $js_libs)) echo 'selected'; ?> value="{{ $jsLib->id }}">{{ $jsLib->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
              </div>


              <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="profile-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <a class="btn btn-success form-control" href="{{ route('tpl.index') }}">Назад</a>
                </div>
                <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-12">
                    <button class="btn btn-success form-control" type="submit">Сохранить</button>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <?php
                    $css_libs = json_decode($tpl->css_libs) ?? [];
                  ?>
                    <div class="item form-group">
                        <select class="csslibs" id='custom-headers' multiple='multiple' name='css_libs[]'>

                            @foreach ($cssLibs ?? [] as $cssLib)
                                <option <?php if (in_array($cssLib->url, $css_libs)) echo 'selected'; ?> value="{{ $cssLib->id }}">{{ $cssLib->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
              </div>



            </form>
          </div>
        </div>
        @foreach ($tpl->fields as $field)
        @if ($field->removable)
          <div class="row">
            <form action="{{ route('field.destroy', $field) }}" method="post">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <div class="col-md-3 col-md-offset-1">
                <h4>
                  {{ $field->type }} {{ $field->name }} = {{ $field->{('var_'.$field->type)} }}
                </h4>
              </div>
              <button class="btn btn-danger">Удалить</button>
            </form>
          </div>
        @endif


        @endforeach
        <form action="{{ route('field.store', ['tpl_id' => $tpl->id]) }}" method="post">
          {{ csrf_field() }}
          <div class="col-md-2 col-sm-2 col-xs-12">
            <select class="form-control search" name="type" placeholder="Тип данных">
              <option value="char">char</option>
              <option value="text">text</option>
              <option value="int">int</option>
            </select>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-12">
            <input class="form-control search" type="text" name="name" placeholder="Имя переменной">
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input class="form-control search" type="text" name="value" placeholder="Значение по-умолчанию">
          </div>
          <div class="col-md-2 col-sm-2 col-xs-12">
            <button class="form-control btn btn-success" type="submit">Добавить</button>
          </div>
        </form>



        <div class="col-md-12 col-sm-12 col-xs-12">
          <form action="{{ route('field.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="name" value="data">
            <input type="hidden" name="type" value="data">
            <input type="hidden" name="tpl_id" value="{{ $tpl->id }}">
            <input type="file" multiple="multiple" name="value[]" accept="image/*">
            <button class="btn btn-success" type="submit">Добавить</button>
          </form>
        </div>

      </div>
    </div>
  </div>
@endsection