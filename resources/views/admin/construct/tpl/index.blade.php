@extends('admin.layouts.app')

@section('content')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        @can('create-tpl')
          <a class="btn btn-success" href="{{ route('tpl.create') }}">Новый шаблон</a>
        @endcan
        <br>
        <?php $i=1; ?>
        @foreach ($tpls as $tpl)
            <?php echo $i++?>
            {{ $tpl->name }}
            @can('edit-tpl')
              <a class="btn btn-success btn-xs" href="{{ route('tpl.edit', $tpl) }}">Редактировать</a>
            @endcan
            <br>


            {{-- {{ dump($tpl) }} --}}
        @endforeach


      </div>
    </div>
  </div>
@endsection