@extends('admin.layouts.app')

@push('stylesheets')
    <link rel="stylesheet" href="/vendors/multi-select/css/multi-select.css">
@endpush

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.jslibs').multiSelect({
              selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Список js-модулей'>",
              selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Подключенные js-модули'>",
              afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                  if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                  }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                  if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                  }
                });
              },
              afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
              },
              afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
              }
            });
            $('.csslibs').multiSelect({
              selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Список css-модулей'>",
              selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Подключенные css-модули'>",
              afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                  if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                  }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                  if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                  }
                });
              },
              afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
              },
              afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
              }
            });
        });
    </script>

    <script src="/vendors/multi-select/js/jquery.quicksearch.js"></script>
    <script src="/vendors/multi-select/js/jquery.multi-select.js"></script>
@endpush

@section('content')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <form action="{{ route('tpl.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6 col-sm-12 col-xs-12">
                <textarea class="form-control search" name="name" id="" cols="30" rows="10" placeholder="Название">{{ old('name') }}</textarea>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <textarea class="form-control search" name="html" id="" cols="30" rows="10" placeholder="HTML">{{ old('html') }}</textarea>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <textarea class="form-control search" name="css" id="" cols="30" rows="10" placeholder="CSS">{{ old('css') }}</textarea>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <textarea class="form-control search" name="js" id="" cols="30" rows="10" placeholder="JS">{{ old('js') }}</textarea>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control search" name="type_id">
                    @foreach ($tplTypes as $type)
                        <option  value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="search" type="file" name="preview">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="item form-group">
                    <select class="jslibs" id='custom-headers' multiple='multiple' name='js_libs[]'>
                        @foreach ($jsLibs as $jsLib)
                            <option value="{{ $jsLib->id }}">{{ $jsLib->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="item form-group">
                    <select class="csslibs" id='custom-headers' multiple='multiple' name='css_libs[]'>
                        @foreach ($cssLibs as $cssLib)
                            <option value="{{ $cssLib->id }}">{{ $cssLib->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <button class="btn btn-success" type="submit">Создать</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection