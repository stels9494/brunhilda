@extends('admin.layouts.app')

@section('content')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">

        @foreach ($cssLibs as $cssLib)
            <div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <h4><ul>{{ $cssLib->name }}</ul></h4>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <h4><ul>{{ $cssLib->url }}</ul></h4>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">

                </div>
            </div>
        @endforeach

            @can('create-tpl')
            <form action="{{ route('cssLib.store') }}" method="post">
                {{ csrf_field() }}
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <input class="form-control search" type="text" name="name" placeholder="Название">
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <input class="form-control search" type="text" name="url" placeholder="URL">
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                    <button class="btn btn-success form-control" type="submit">Добавить</button>
                </div>
            </form>
        @endcan

      </div>
    </div>
  </div>
@endsection