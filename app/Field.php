<?php

namespace App;

use App\Field;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $table = 'fields';
    public $timestamps = false;
    protected $guarded = [];

    public function tpl()
    {
        return $this->belongsTo('App\Tpl');
    }

    /*создание поля data для шаблона*/
    public static function createDataField($tpl)
    {
        return Field::create([
            'type' => 'data',
            'var_data' => json_encode([]),
            'tpl_id' => $tpl->id,
            'name' => 'data',
            'removable' => false
        ]);
    }
}
