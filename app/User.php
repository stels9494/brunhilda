<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Site;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Events\UserCreated;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'second_token', 'activate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*роли и их права*/
    // private $listPermissions = [
    //     'admin' => [
    //         'create-tpl',
    //         'edit-tpl',
    //         'remove-tpl',
    //         'add-block',
    //         'remove-block',
    //         'edit-block',
    //         'create-site',
    //         'remove-site',
    //         'create-page',
    //         'remove-page'
    //     ],
    //     'tpl-developer' => [
    //         'create-tpl',
    //         'edit-tpl'
    //     ],
    //     'product-manager' => [
    //         'add-block',
    //         'remove-block',
    //         'edit-block',
    //         'create-page',
    //         'remove-page'
    //     ],
    //     'user' => [
    //         'edit-block'
    //     ]
    // ];



    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /*создание пользователя с ролью product manager и его правами*/
    public static function createProductManager($site_id, $email)
    {
        //проверить, что для данного сайта нет product manager
        $users = Site::where('id', $site_id)->with('users');
        foreach ($users as $user)
        {
            if ($user->hasRole('product_manager'))
                return false;
        }
        //создать пользователя c ролью и правами product manager
        $result = self::create([
            'email' => $email,
            'second_token' => bcrypt($email),
            'activate' => false,
            'password' => null
        ]);
        if (!$result) return false;
        $result->syncRole(['product_manager']);
        $result->syncPermission($result->permission['product_manager']);
        return $result;
    }

    /*создание пользователя с ролью user и назначение ему соответствующих прав*/
    public static function createUser($site_id, $email)
    {
        $result = self::create([
            'email' => $email,
            'second_token' => bcrypt($email),
            'activate' => false,
            'password' => null
        ]);
        if (!$result) return false;
        $result->syncRole(['user']);
        $result->syncPermission($result->permission['user']);
        return $result;
    }

    /*создание пользователя с ролью tpl_developer и назначе ему соответствующих прав*/
    public static function createTplDeveloper($email)
    {
        $result = self::create([
            'email' => $email,
            'second_token' => bcrypt($email),
            'activate' => false,
            'password' => null
        ]);
        if (!$result) return false;
        $result->syncRole(['tpl_developer']);
        $result->syncPermission($result->permission['tpl_developer']);
        return $result;
    }







    /******************RELATION**********************/
    // public function sites()
    // {
    //     return $this->belongsToMany('App\Site', 'site_user');
    // }

    public function accounts()
    {
        return $this->hasMany('App\Account');
    }
}
