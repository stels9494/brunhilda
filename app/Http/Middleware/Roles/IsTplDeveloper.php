<?php

namespace App\Http\Middleware\Roles;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsTplDeveloper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //проверить что пользователь авторизован
        if (!Auth::check())
            return redirect()->route('main.index');
        //получить модель user
        $user = Auth::user();
        if (!$user->hasRole('tpl-developer'))
            return redirect()->route('main.index');
        //если он tpl-developer - пропустить
        return $next($request);
    }
}
