<?php

namespace App\Http\Middleware\Roles;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //проверить что пользователь авторизован
        if (!Auth::check())
            return redirect()->route('main.index');
        //получить модель user
        $user = Auth::user();
        if (!$user->hasRole('admin'))
            return redirect()->route('main.index');
        //если он admin - пропустить
        return $next($request);
    }
}
