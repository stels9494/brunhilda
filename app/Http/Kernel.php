<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
            //сам добавил
            \Barryvdh\Cors\HandleCors::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'is.auth' => \App\Http\Middleware\IsAuth::class,
        //роли
        'is.admin' => \App\Http\Middleware\Roles\IsAdmin::class,
        'is.product.manager' => \App\Http\Middleware\Roles\IsProductManager::class,
        'is.tpl.developer' => \App\Http\Middleware\Roles\IsTplDeveloper::class,
        'is.user' => \App\Http\Middleware\Roles\IsUser::class,
        //права
        'can.add.block' => \App\Http\Middleware\Permissions\CanAddBlock::class,
        'can.create.page' => \App\Http\Middleware\Permissions\CanCretaePage::class,
        'can.create.site.' => \App\Http\Middleware\Permissions\CanCreateSite::class,
        'can.create.tpl' => \App\Http\Middleware\Permissions\CanCreateTpl::class,
        'can.edit.block' => \App\Http\Middleware\Permissions\CanEditBlock::class,
        'can.edit.tpl' => \App\Http\Middleware\Permissions\CanEditTpl::class,
        'can.remove.block' => \App\Http\Middleware\Permissions\CanRemoveBlock::class,
        'can.remove.page' => \App\Http\Middleware\Permissions\CanRemovePage::class,
        'can.remove.site' => \App\Http\Middleware\Permissions\CanRemoveSite::class,
        'can.remove.tpl' => \App\Http\Middleware\Permissions\CanRemoveTpl::class,
        'can.show.tpl' => \App\Http\Middleware\Permissions\CanShowTpl::class,
        'can.show.visual.construct' => \App\Http\Middleware\Permissions\CanShowVisualConstruct::class,
        //jwt
        'jwt.auth' => 'Tymon\JWTAuth\Middleware\GetUserFromToken',
        'jwt.refresh' => 'Tymon\JWTAuth\Middleware\RefreshToken',
    ];
}
