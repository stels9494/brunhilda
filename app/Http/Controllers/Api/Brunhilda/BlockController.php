<?php

namespace App\Http\Controllers\Api\Brunhilda;


use App\Block;
use App\BlockField;
use App\Data;
use App\Account;
use App\Http\Controllers\Controller;
use App\Services\DataPreparationService;
use App\Services\RenderService;
use App\Tpl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class BlockController extends Controller
{

    /*вывод блоков страницы*/
    public function blocks(Request $request, $site_id, $page_id)
    {
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account) return "false";
        }
        $blocks = Block::getBlocksPage($page_id);        //запрос блоков страницы
        //подготовка данных для фронта
        $data = [];
        foreach ($blocks ?? [] as $block)
        {
            $render = RenderService::renderBlock($block);
            $data[] = DataPreparationService::forGetBlocksPage($render, $block);
        }
        return $data;
    }


    public function createBlock(Request $request, $site_id, $page_id)
    {
        //проверить, что пользователь либо админ, либо у него есть право на создание блоков
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('create-block'))
                return "false";
        }

        /*определяем или переобределяем порядкового номера блоков страницы*/


        //создание подготовленного блока
        $data = DataPreparationService::forCreateBlock($site_id, $page_id, $request->tpl_id, $request->sort);
        $block = Block::create($data);
        //теперь нужно скопировать филды для блока из филдов шаблона
        $block->copyFieldFromTpl($request->tpl_id);
        //формируем ответ заданного формата, для отображения во фронте
        //рндеринг
        $render = RenderService::renderBlock($block);
        $data = DataPreparationService::forGetBlocksPage($render, $block);
        return $data;
    }

    public function deleteBlock(Request $request, $site_id, $page_id, $block_id)
    {
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('remove-block'))
                return "false";
        }
        Block::find($block_id)->delete();
    }


    /*смена позиций двух блоков*/
    public function moveBlocks(Request $request, $site_id, $page_id)
    {
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('edit-block'))
                return "false";
        }
        $blocks = Block::where(function ($query) use ($request) {
                        $query->where('id', $request->block_id_1)->orWhere('id', $request->block_id_2);
                    })->where([
                        ['page_id', $page_id],
                        ['site_id', $site_id]
                    ])->get();
        if ($blocks->count() == 2)
        {
            $sortOfFirst = $blocks[0]->sort;
            $blocks[0]->sort = $blocks[1]->sort;
            $blocks[0]->save();
            $blocks[1]->sort = $sortOfFirst;
            $blocks[1]->save();
        }
    }




    /*создание копии блока*/
    // public function duplicateBlock(Request $request, $site_id, $page_id, $block_id)
    private function duplicate(Request $request)
    {
        //поиск исходного блока, который будет скопирован
        // $source_block = Block::where('id', $request->block_id)
        //                     ->with('blockFields')
        //                     ->get()
        //                     ->first();
        $source_block = Block::with('blockFields')
                            ->find($request->block_id);
        $user = Auth::user();
        //проверка прав
        if (!$user->hasRole('admin'))
        {
            $site_id = $source_block->site_id;
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('create-block'))
                return "false";
        }
        //поиск блоков, у которых нужно проинкрементить sort
        $blocks = Block::where('page_id', $source_block->page_id)
                        ->where('sort', '>=', $source_block->sort)
                        ->orderBy('sort')
                        ->get();
        //меняем sort у блоков
        foreach ($blocks as &$block)
        {
            $block->sort++;
            $block->save();
        }

        //формируем данные для нового блока

        $data = $source_block->toArray();
        unset($data['id']);
        unset($data['block_fields']);
        $data['sort']++;

        //создаем новый блок
        $copy_block = Block::create($data);

        //скопировать все blockFields исходного блока
        foreach ($source_block->blockFields as $block_field)
        {
            //подготовка данных
            $data = $block_field->toArray();
            unset($data['id']);
            $data['block_id'] = $copy_block->id;
            //записываем
            BlockField::create($data);
        }


        //копирование файлов
        foreach ($source_block->getDataBlocks() as $datum)
        {
            //формироавние имени файла
            $copy_data = $copy_block->createDataDefault();
            $ext = substr($datum->full_name, strrpos($datum->full_name, '.') + 1);
            $new_folder = (int)($copy_data->id/1000) + 1;
            $filename = $new_folder.'_'.$copy_data->id.'_'.time().'.'.$ext;
            //определение расположения копируемого файла
            $old_folder = substr($datum->full_name, 0, stripos($datum->full_name, '_'));
            //копирование
            Storage::copy('public/data/'.$old_folder.'/'.$datum->full_name, 'public/data/'.$new_folder.'/'.$filename);

            //заносим новые данные в базу
            $copy_data->update([
                'full_name' => $filename,
                'mimetype' => $datum->mimetype,
            ]);
        }

        //запись id Data в data_field для блока

        $block_field = $copy_block->getDataBlockField();
        $block_field->var_data = json_encode($copy_block->getDataBlocks()->pluck('id'));
        $block_field->save();


        /**********Конец нового кода****************/

        //рендеринг блока
        $render = RenderService::renderBlock($copy_block);
        return DataPreparationService::forGetBlocksPage($render, $copy_block);
    }

    /*удалить эту функцию*/

    public function deleteAllBlocks(Request $request, $page_id)
    {
        $blocks = Block::where('page_id', $page_id)->get();
        foreach ($blocks as $item)
            $item->delete();
    }








    /*возвращает все филды блока*/
    private function getBlockFields($request)
    {
        $block_id = $request->block_id;
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $site_id = Block::find($block_id)->site_id;
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account) return "false";
        }
        $result = [];
        //запрос всех block_fields
        // dd($block_id);
        $blockFields = Block::find($block_id)->getBlockFields();
        //найти поля data
        $blockFields = $blockFields->where('type', '<>', 'data');
        foreach ($blockFields as $blockField)
        {
            $result[] = [
                'name' => $blockField->name,
                'type' => $blockField->type,
                'value' => $blockField->{'var_'.$blockField->type},
            ];
        }
        //подготовить данные (type, value, name)
        // return $data = Data::getDataBlock($block_id)->pluck('full_name')->toArray();
        $data = Data::getDataBlock($block_id)->toArray();
        //сформировать прямые ссылки на документы
        foreach ($data as &$datum)
        {
            $folder = substr($datum['full_name'], 0, stripos($datum['full_name'], '_'));
            $datum['full_name'] = URL::to('/').'/storage/data/'.$folder.'/'.$datum['full_name'];
        }
        $result[] = [
            'name' => 'data',
            'type' => 'data',
            'value' => $data,
        ];
        return $result;
    }

    /**/
    private function change(Request $request)
    {
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $site_id = Block::find('block_id')->site_id;
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('edit-block'))
                return "false";
        }
        $block_field = BlockField::where('block_id', $request->block_id)
                                ->where('name', $request->name)
                                ->get()
                                ->first();
        //занести новые данные
        if ($block_field->data == 'data') return;
        $block_field->{'var_'.$block_field->type} = $request->value;
        $block_field->save();

        //если запрашивает блок - отрендерить
        if ($request->isNeedToIncludeBlockInResponse)
        {
            $block = Block::with('blockFields')->find($request->block_id);
            $render = RenderService::renderBlock($block);
            return DataPreparationService::forGetBlocksPage($render, $block);
        }
        return;
    }

/****************************Управляющие методы**********************************/


    public function index(Request $request)
    {
        switch ($request->action)
        {
            case 'getBlockFields': //url: http://192.168.0.9/api/dataContent?action=getBlockFields&block_id=259
                return $this->getBlockFields($request);
                break;
            // case 'change': //{block_id, name, value}
            //     $this->change($request);
            //     break;
       }
        return;
    }

    public function store(Request $request)
    {
        switch ($request->action)
        {
            // case 'delete'://{block_id}
                // return $this->deleteBlock()
            case 'duplicate'://{block_id}
                return $this->duplicate($request);

            case 'change': //{block_id, name, value}
                return $this->change($request);
        }
        return;
    }
}
