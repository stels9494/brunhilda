<?php

namespace App\Http\Controllers\Api\Brunhilda;

use App\Account;
use App\Http\Controllers\Controller;
use App\Site;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{

    public function sites(Request $request)
    {
        $user = Auth::user();
        //для админа
        if ($user->hasRole('admin'))
        {
            $query = Site::paginate($request->qty ?? 20)->toArray();
            foreach ($query['data'] as &$site){
                $site['permissions'][] = 'all';
            }
            $result = ['paginate' => $query] + ['sites' => $query['data']];
        }else{
            //для остальных пользователей
            $query = Account::where('user_id', $user->id)->with('site')->paginate($request->qty ?? 20);
            foreach ($query as $key => $account)
            {
                $result['sites'][$key] = $account->site;
                $result['sites'][$key]['permissions'] = $account->getAllPermissions()->pluck('name')->toArray();
            }
            $result['paginate'] = $query->toArray();
        }
        //общая часть
        $result['user_info'] = [
            'email' => $user->email,
            'name' => $user->name,
        ];
        unset($result['paginate']['data']);
        return $result;
    }
}
