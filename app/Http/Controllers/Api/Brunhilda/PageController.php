<?php

namespace App\Http\Controllers\Api\Brunhilda;

use App\Account;
use App\Block;
use App\Http\Controllers\Controller;
use App\Page;
use App\Services\DataPreparationService;
use App\Services\RenderService;
use App\TplType;
use App\Validations\Api\PageValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{

    /*вывод всех страниц сайта*/
    public function pages(Request $request, $site_id=null)
    {
        /*

        */
        $user = Auth::user();
        //если админ - получить страницы текущего сайта
        //если не админ - проверить, есть ли у пользователя аккаунт
        if (!$user->hasRole('admin'))
        {
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account) return 'false';
            $query = $account->site->pages()->paginate($request->qty ?? 20);
        }else{
            $query = Page::where('site_id', $site_id)->paginate($request->qty ?? 20);
        }
        //проверка, что текущая страница не ушла за пределы диапазона
        if ($query->currentPage() > $query->lastPage()){
            $request->merge(['page' => $query->lastPage()]);
            $query = Account::where('user_id', $user->id)->where('site_id', $site_id)->first()->site->pages()->paginate($request->qty ?? 20);
        }
        $result['paginate'] = $query->toArray();
        $result['pages'] = $query->items();
        unset($result['paginate']['data']);
        $result['permissions'] = $user->hasRole('admin')? ['all']: array_pluck($account->getAllPermissions(), 'name');
        return $result;
    }


    /*создание новой страницы сайта*/
    public function createPage(Request $request, $site_id)
    {
        //проверка прав
        $user = Auth::user();
        if (!$user->hasRole('admin')){
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('create-page')) return "false";
        }
        //валидация
        if (!$data = PageValidation::createPage($request)) return "false";
        //выполнение запроса
        Page::createNewPage($data, $site_id);//создание новой страницы
        return $this->pages($request, $site_id);//запрос всех страниц
    }

    /*Вывод всех блоков текущей страницы и всех шаблонов на сайте*/
    public function blocksAndTpls(Request $request, $site_id, $page_id)
    {
        //проверить, что пользователь прикреплен к данному сайту и вывести
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account) return "false";
        }
        //запрос блоков страницы
        $blocks = Block::getBlocksPage($page_id);
        //подготовка данных для фронта
        $data = [];
        foreach ($blocks ?? [] as $block)
        {
            $render = RenderService::renderBlock($block);
            $data[] = DataPreparationService::forGetBlocksPage($render, $block);
        }

        $result = [
            'tpls' => TplType::getTypesWithTemplates(),
            'blocks' => $data,
            'permissions' => ($user->hasRole('admin'))? ['all']: array_pluck($account->getAllPermissions(), 'name'),
        ];
        return $result;
    }

    /*
        Удаление страницы сайта
        input:
            $request->token
            $request->page_id
        return:
            bool
    */
    public function removePage($request)
    {
        $user = Auth::user();
        if (!$page = Page::find($request->page_id)) return "false";
        $site_id = $page->site_id;
        if (!$user->hasRole('admin'))
        {
            $account = Account::where('user_id', $user->id)->where('site_id', $page->site_id)->first();
            if (!$account || !$account->hasPermissionTo('remove-page')) return "false";
        }
        return ($page->delete())? $this->pages($request, $site_id): "false";
    }

    public function index(Request $request)
    {
        switch ($request->action)
        {
            default:
                return "Action not switched.";
        }
    }

    public function store(Request $request)
    {
        switch ($request->action)
        {
            case 'remove'://token, page_id
                return $this->removePage($request);
            default:
                return "Action not switched.";
        }
        return;
    }
}
