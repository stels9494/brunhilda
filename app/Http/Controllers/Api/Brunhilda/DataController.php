<?php

namespace App\Http\Controllers\Api\Brunhilda;

use App\Block;
use App\BlockField;
use App\Data;
use App\Account;
use App\Http\Controllers\Controller;
use App\Services\DataPreparationService;
use App\Services\RenderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Storage;

class DataController extends Controller
{




    private function uploadDataBlock($request)
    {
        if (!$request->image && !$request->url)
            return "false";
        $block_id = $request->block_id;
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $site_id = Block::find($block_id)->site_id;
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('edit-block'))
                return "false";
        }

//создать запись, получить его id
//найти блок, вызвать метод создания пустого data для этого блока


        if (!$block = Block::findOrFail($block_id)) return "false";
        $data = $block->createDataDefault(); //создать такой метод
        $current_id = $data->id;

        $folder_name = (int)($current_id / 1000 + 1);
        //если загрузка требуется по url
        if ($request->url)
        {

            /*все загруженные должны быть определенного формата и блок должен существовать*/
            $validator = Validator::make($request->all(), [
                'url' => 'required',
                'data_id' => 'integer',
                'block_id' => 'integer|exists:blocks,id',
            ]);
            if($validator->fails())
            {
                return "false";
            }

            $contents = file_get_contents($request->url);
            //извлечение расширения изображения
            $ext = substr($request->url, strrpos($request->url, '.') + 1);
            //формируется имя('папка_id_время.extension')

            $new_name = $folder_name.'_'.$current_id.'_'.time().'.'.$ext;

            //помещение изображения в папку долгосрочного хранения
            Storage::put('public/data/'.$folder_name.'/'.$new_name, $contents);

            //валидация изображения

            $size = Storage::size('public/data/'.$folder_name.'/'.$new_name);
            $mime = Storage::mimeType('public/data/'.$folder_name.'/'.$new_name);
            $arMimes = [
                'image/png',
                'image/jpeg',
                'image/gif',
                'application/x-bittorrent',
                'application/pdf',
                'application/zip',
                'application/gzip',
            ];

            if ($size > 1024*1024 || !in_array($mime, $arMimes))
            {
                //удалить файл
                Storage::delete('public/data/'.$folder_name.'/'.$new_name);
                return;
            }

            // $full_name = 'storage/data/'.$folder_name.'/'.$new_name;
        }
        //если загрузка требуется ajax-ом
        elseif ($request->image)
        {

            /*все загруженные должны быть определенного формата и блок должен существовать*/
            $request->validate([
                'image' => 'mimes:jpeg,png,gif,x-bittorrent,pdf,zip,gzip|max:1000',
                'block_id' => 'integer|min:1',
            ]);

            $new_path = storage_path('app/public/data/'.$folder_name);

            $new_name = $folder_name.'_'.$current_id.'_'.time().'.'.$request->image->getClientOriginalExtension();

            // $new_name = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move($new_path, $new_name);
            // $full_name = 'storage/data/'.$folder_name.'/'.$new_name;
        }
        $mimetype = Storage::mimeType('public/data/'.$folder_name.'/'.$new_name);
        // нужно занести новый адрес в дату, получить id, занести в data field блока



        // $id_data = Data::addData($new_name, $block_id, $block->tpl_id, $mimetype)->id;
        $data->updateData($new_name, $mimetype);

        $block_field = $block->getDataBlockField();
        $ar = (array)json_decode($block_field->var_data);
        $ar[] = $data->id;
        $block_field->var_data = json_encode($ar);
        $block_field->save();
        // отрендерить блок
        $render = RenderService::renderBlock($block);
        $result = DataPreparationService::forGetBlocksPage($render, $block);
        return $result;
    }


    private function changeDataInBlock($request)
    {
        if (!$request->image && !$request->url)
            return "false";

        if (!((int)$request->data_id))
            return $this->uploadDataBlock($request);

        $block_id = $request->block_id;
        $data_id = $request->data_id;
        $current_id = $data_id;
        $folder_name = (int)($current_id / 1000 + 1);
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $site_id = Block::find($block_id)->site_id;
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('edit-block'))
                return "false";
        }
        if ($request->url)
        {
            /*все загруженные должны быть определенного формата и блок должен существовать*/
            $validator = Validator::make($request->all(), [
                'url' => 'required',
                'data_id' => 'integer|min:1|exists:data,id',
                'block_id' => 'integer|min:1|exists:blocks,id',
            ]);
            if($validator->fails())
            {
                return "false";
            }

            $contents = file_get_contents($request->url);
            //извлечение расширения изображения
            $ext = substr($request->url, strrpos($request->url, '.') + 1);
            //формируется имя('папка_id_время.extension')

            $new_name = $folder_name.'_'.$current_id.'_'.time().'.'.$ext;

            // return 'public/data/'.$folder_name.'/'.$new_name;
            //помещение изображения в папку долгосрочного хранения
            Storage::put('public/data/'.$folder_name.'/'.$new_name, $contents);
            //валидация изображения

            $size = Storage::size('public/data/'.$folder_name.'/'.$new_name);
            $mime = Storage::mimeType('public/data/'.$folder_name.'/'.$new_name);
            $arMimes = [
                'image/png',
                'image/jpeg',
                'image/gif',
                'application/x-bittorrent',
                'application/pdf',
                'application/zip',
                'application/gzip',
            ];

            if ($size > 1024*1024 || !in_array($mime, $arMimes))
            {
                //удалить файл
                Storage::delete('public/data/'.$folder_name.'/'.$new_name);
                return "false";
            }
            // $full_name = 'storage/data/'.$folder_name.'/'.$new_name;
        }
        //если загрузка требуется ajax-ом
        elseif ($request->image)
        {

            /*все загруженные должны быть определенного формата и блок должен существовать*/
            $validator = Validator::make($request->all(), [
                'image' => 'mimes:jpeg,png,gif,x-bittorrent,pdf,zip,gzip|max:1000',
                'block_id' => 'integer|min:1|exists:blocks,id',
            ]);
            if($validator->fails())
            {
                return "false";
            }

            $new_path = storage_path('app/public/data/'.$folder_name);

            $new_name = $folder_name.'_'.$current_id.'_'.time().'.'.$request->image->getClientOriginalExtension();

            // $new_name = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move($new_path, $new_name);
            // $full_name = 'storage/data/'.$folder_name.'/'.$new_name;
        }
        $data = Data::find($data_id);
        Storage::delete('public/data/'.$folder_name.'/'.$data->full_name);
        $data->mimetype = Storage::mimeType('public/data/'.$folder_name.'/'.$new_name);
        $data->full_name = $new_name;
        $data->save();


        $block = Block::find($block_id);
        // отрендерить блок
        $render = RenderService::renderBlock($block);
        $result = DataPreparationService::forGetBlocksPage($render, $block);
        return $result;
    }

    private function deleteDataFromBlock($request)
    {
        $user = Auth::user();
        if (!$user->hasRole('admin'))
        {
            $site_id = BlockField::with('block')->find($request->block_field_id)->site_id;
            $account = Account::where('user_id', $user->id)->where('site_id', $site_id)->first();
            if (!$account || !$account->hasPermissionTo('edit-block'))
                return "false";
        }
        // $block_id = $request->block_id;
        // получить путь файла, удалить его и запись из data и из block_field с типом data
        // $data = Data::find($request->data_id);
        // $name = $data->full_name;
        // $folder = substr($name, 0, stripos($name, '_'));


        // $block_field = BlockField::getDataBlockField($block_id);
        // $ar = json_decode($block_field->var_data);
        //простите меня за такой грубейший костыль, но иначе никак
        // print_r($ar);
        // $new_ar = [];
        // foreach ($ar as $key => $value)
        // {

        //     if ($data->id == $value) continue;
        //     $new_ar[$key] = $value;
        // }
        // $ar = $new_ar;
        // $data->delete();
        // Storage::delete('public/data/'.$folder.'/'.$name);
        // $block_field->var_data = json_encode($ar);
        // $block_field->save();

        //удаление записи о файле в Data
        Data::find($request->data_id)->delete();
        //рендер блока
        $block = Block::with('blockFields')->find($request->block_id);
        $render = RenderService::renderBlock($block);
        return DataPreparationService::forGetBlocksPage($render, $block);
    }



    public function index(Request $request)
    {

    }

    public function store(Request $request)
    {
        switch ($request->action)
        {
            //загрузка файлов в data
            case 'upload'://{block_id, image или url файла}
                return $this->uploadDataBlock($request);

            //заменить файл в data
            case 'change':// {block_id, image или url файла}
                return $this->changeDataInBlock($request);
            //удалить файл
            case 'delete'://{block_field_id, data_id}
                return $this->deleteDataFromBlock($request);
        }
    }
}
