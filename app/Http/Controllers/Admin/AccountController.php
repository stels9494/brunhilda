<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\Http\Controllers\Controller;
use App\Site;
use App\User;
use App\Validations\AccountValidation;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $accounts = Account::paginate($request->qty ?? 20);
        return view('admin.userManagment.accounts.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::where('guard_name', 'account')->get();
        return view('admin.userManagment.accounts.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = AccountValidation::store($request);
        $account = Account::create($data);
        $account->syncPermissions(request()->permissions ?? []);
        return redirect()->route('accounts.edit', $account)->with('flash_message', 'Аккаунт Создан.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        return view('admin.userManagment.accounts.show', compact('account'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        $permissions = Permission::where('guard_name', 'account')->get();
        return view('admin.userManagment.accounts.edit', compact('account', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        $data = AccountValidation::update($request);
        $account->update($data);
        $account->syncPermissions(request()->permissions ?? []);
        return redirect()->route('accounts.edit', $account)->with('flash_message', 'Изменения сохранены.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        $account->delete();
        return redirect()->route('accounts.index');
    }

}
