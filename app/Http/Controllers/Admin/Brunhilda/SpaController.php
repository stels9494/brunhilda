<?php

namespace App\Http\Controllers\Admin\Brunhilda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class SpaController extends Controller
{
  public function index(Request $request)
  {
    $token = $request->session()->get('jwt_token');
    return view('admin.construct.spa', compact('token'));
  }
}
