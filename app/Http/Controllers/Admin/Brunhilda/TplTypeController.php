<?php

namespace App\Http\Controllers\Admin\Brunhilda;

use App\Http\Controllers\Controller;
use App\TplType;
use Illuminate\Http\Request;

class TplTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tplTypes = TplType::all();
        return view('admin.construct.tplType.index', compact('tplTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string'
        ]);
        $result = TplType::create($data);
        return back()->with('flash_message', 'Новый тип добавлен.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => 'nullable|string'
        ]);
        $result = TplType::find($id)->update($data);
        return back()->with('flash_message', 'Изменения сохранены.');
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function __construct()
    {
        $this->middleware('can.create.tpl')->only(['create', 'store']);
        $this->middleware('can.edit.tpl')->only(['edit', 'update']);
        $this->middleware('can.remove.tpl')->only(['destroy']);
    }
}
