<?php

namespace App\Http\Controllers\Admin\Brunhilda;

use App\Http\Controllers\Controller;
use App\CssLib;
use Illuminate\Http\Request;

class CssLibController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cssLibs = CssLib::all();
        return view('admin.construct.cssLib.index', compact('cssLibs'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = CssLib::create([
            'name' => $request->name,
            'url' => $request->url
        ]);
        return back()->with('flash_message', 'Библиотека добавлена в список.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => 'nullable|string',
            'url' => 'nullable|string'
        ]);
        $result = CssLib::find($id)->update($data);
        return redirect()->route('CssLib.index')>with('flash_message', 'Новый тип добавлен.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function __construct()
    {
        $this->middleware('can.create.tpl')->only(['create', 'store']);
        $this->middleware('can.edit.tpl')->only(['edit', 'update']);
        $this->middleware('can.remove.tpl')->only(['destroy']);
    }
}
