<?php

namespace App\Http\Controllers\Admin\Brunhilda;

use App\Block;
use App\BlockField;
use App\Field;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FieldController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //валидация данных
        $data = $request->validate([
            'type' => 'required|string',
            'name' => 'required|string',
            'value' => 'required|string',
            'tpl_id' => 'required|integer|exists:tpls,id',
        ]);

        //создание нового field
        $data['var_'.$data['type']] = $data['value'];
        unset($data['value']);
        $field = Field::create($data);

        //поиск блоков, которые используют текущий шаблон
        $blocks = Block::where('tpl_id', $data['tpl_id'])->get();

        //для каждого блока делаем копию в BlockField




        //для каждого блока создаем новый field
        unset($field['id']);
        foreach ($blocks as $block)
        {
            $data = $field->toArray();
            $data += [
                'site_id' => $block->site_id,
                'page_id' => $block->page_id,
                'block_id' => $block->id,
            ];
            BlockField::create($data);
        }
        return back()->with('flash_message', 'Поле шаблона добавлено.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $field = Field::find($id);

        //если поле неудаляемое, то не удалять
        if (!$field->removable)
            return back()->with('flash_error', 'При удалении произошла ошибка.');

        //BlockField блоков, которые построены на шаблоне удаляемого field
        $blockFields = BlockField::where('tpl_id', $field->tpl_id)
                                ->where('name', $field->name)
                                ->where('type', $field->type)
                                ->get();
        foreach ($blockFields as &$block)
            $block->delete();

        //удалить поле из шаблона
        $field->delete();
        return back()->with('flash_message', 'Переменная шаблона удалена.');
    }

}
