<?php

namespace App\Http\Controllers\Admin\Brunhilda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sites = Site::paginate($request->quantity ?? 50);
        return view('admin.construct.sites.index', compact('sites'));
    }

    /**
     * Добавление нового сайта
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'prefix' => 'nullable|string'
        ]);

        Site::create($data);
        return redirect()->back()->with('flash_message', 'Сайт добавлен.');
    }

    /**
     * отображение страниц сайта и пользователей, которые имеют доступ к управлению
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $site = Site::find($id);
        return view('admin.construct.sites.show', compact('site'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        switch ($request->action)
        {
            //включение/выключение сайта
            case 'switch':
                $site->activate = $request->status;
                $site->save();
                break;
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
