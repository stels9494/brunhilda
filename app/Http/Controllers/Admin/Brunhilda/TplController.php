<?php

namespace App\Http\Controllers\Admin\Brunhilda;

use App\CssLib;
use App\Field;
use App\Http\Controllers\Controller;
use App\JsLib;
use App\Tpl;
use App\TplType;
use Illuminate\Http\Request;

class TplController extends Controller
{


                        // if (isset($n->getMedia('news')[0])) {
                        //     echo URL::to('/').$n->getMedia('news')[0]->getUrl('indexAnons');
                        // } else {
                        //     echo "/images/imageNotFound.jpg";
                        // }


    /**
     * отображение всех шаблонов
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        извлечь шаблон из бд
        извлчь все его переменные
        сформировать в один массив все переменные
        отрендерить
        */
        // $tpl = Tpl::find(1);
        // $replace = [];
        // $search = [];
        // foreach ($tpl->fields as $field)
        // {
        //     // dd('{{ $'.$field->name.' }}');
        //     $search[] = '{{ $'.$field->name.' }}';
        //     $replace[] = $field->{'var_'.$field->type};
        // }
        // echo $html = str_replace($search, $replace, $tpl->html);

        $tpls = Tpl::all();
        return view('admin.construct.tpl.index', compact('tpls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tplTypes = TplType::all();
        $jsLibs = JsLib::all();
        $cssLibs = CssLib::all();
        return view('admin.construct.tpl.create', compact('tplTypes', 'jsLibs', 'cssLibs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'css' => 'nullable|string',
            'html' => 'nullable|string',
            'js' => 'nullable|string',
            'type_id' => 'required|integer|min:1',
            'js_libs.*' => 'nullable|integer',
            'css_libs.*' => 'nullable|integer',
        ]);
        //получение адресов библиотек js
        $data['js_libs'] = json_encode(JsLib::whereIn('id', $request->js_libs ?? [])->pluck('url'));
        $data['css_libs'] = json_encode(CssLib::whereIn('id', $request->css_libs ?? [])->pluck('url'));

        $tpl = Tpl::create($data);
        //создание обязательных переменных
        Field::createDataField($tpl);

        //загрузка превью
        if ($tpl && $request->hasFile('preview')) {
            $tpl->addMedia($request->preview)->toMediaCollection('preview');
            $linkImage = $tpl->getMedia('preview')->first()->getUrl('preview');
            $tpl->preview = parse_url($linkImage)['path'];
            $tpl->save();
        }

        return redirect()->route('tpl.edit', $tpl->id)->with('flash_message', 'Шаблон создан.');
    }

    public function edit(Tpl $tpl)
    {
        $tplTypes = TplType::all();
        $jsLibs = JsLib::all();
        $cssLibs = CssLib::all();
        return view('admin.construct.tpl.edit', compact('tpl', 'tplTypes', 'jsLibs', 'cssLibs'));
    }

    public function update(Request $request, Tpl $tpl)
    {

        $data = $request->validate([
            'name' => 'nullable|string',
            'css' => 'nullable|string',
            'html' => 'nullable|string',
            'js' => 'nullable|string',
            'type_id' => 'nullable|integer|min:1',
            'js_libs.*' => 'nullable|integer',
            'css_libs.*' => 'nullable|integer',
        ]);
        $data['js_libs'] = json_encode(JsLib::whereIn('id', $request->js_libs ?? [])->pluck('url'));
        $data['css_libs'] = json_encode(CssLib::whereIn('id', $request->css_libs ?? [])->pluck('url'));
        $result = $tpl->update($data);

        return back()->with('flash_message', 'Изменения сохранены.');
    }

    public function __construct()
    {
        $this->middleware('can.create.tpl')->only(['create', 'store']);
        $this->middleware('can.edit.tpl')->only(['edit', 'update']);
        $this->middleware('can.remove.tpl')->only(['destroy']);
    }

}
