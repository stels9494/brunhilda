<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Отображает весь список прав и ролей
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //получить роли с их правами
        $roles = Role::all();
        $permissions = Permission::all();
        return view('admin.userManagment.roles.index', compact('roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = RoleValidation::store($request);
        switch ($request->name) {
            case 'role':
                Role::create(['name' => $request->role]);
                break;
            case 'permission':
                Permission::create(['name' => $request->permission]);
                break;
        }
        return back()->with('flash_message', 'Успешно добавлено.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $role = Role::where('id', $id)->with('permissions')->get()->first();
        // $permissions = Permission::all();
        // return view('admin.userManagment.roles.show', compact('role', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->name ?? [];
        Role::find($id)->syncPermissions($data);
        return back()->with('flash_message', 'Изменения сохранены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        switch ($request->name) {
            case 'role':
                Role::find($id)->delete();
                $message = "Роль была удалена.";
                break;
            case 'permission':
                Permission::find($id)->delete();
                $message = "Право было удалено.";
                break;
        }
        return redirect()->route('roles.index')->with('flash_message', $message);
    }
}
