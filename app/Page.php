<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    public $timestamps = true;
    protected $guarded = [];
    protected $hidden = ['site_id', 'updated_at', 'created_at'];

    /*возвращает всю информацию для страницы по заданному url*/
    public static function getPageInfo($url)
    {
        return self::where('url', $url)->get();
    }


    /*создает новую страницу с заданным заголовком и url*/
    public static function createPage($title, $url)
    {
        return self::insert(['title' => $title, 'url' => $url]);
    }


    /*******************************RELATIONSHIP************************************/

    public function blocks()
    {
        return $this->hasMany('App\Block');
    }

    public function blockFields()
    {
        return $this->hasMany('App\BlockField');
    }

    public function site()
    {
        return $this->belongsTo('App\Site');
    }


    /*************************************CUSTOM FUNCTION****************************************/

    /*возвращает все страницы сайта по его id*/
    public static function getPagesSite($site_id)
    {
        return self::where('site_id', $site_id)->get();
    }

    public static function createNewPage($data, $site_id)
    {
        return self::create($data);
    }

}
