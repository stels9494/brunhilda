<?php
namespace App\Services;

use App\Block;

class DataPreparationService
{

    /*для получения всех блоков страницы в Api\BlockController*/
    public static function forGetBlocksPage($data, $block)
    {
        $data += [
            'id' => $block->id,
            'jsLibs' => json_decode($block->tpl->js_libs),
            'cssLibs' => json_decode($block->tpl->css_libs),
            'sort' => $block->sort
        ];
        return $data;
    }

    /*для создания нового блока в Api\BlockController*/
    public static function forCreateBlock($site_id, $page_id, $tpl_id, $sort = null)
    {
        $max = Block::getMaxSortOnPage($page_id);
        $data = [];
        //если блок самый первый на странице
        if (!$max)
        {
            $data = [
                'sort' => 1,
                'page_id' => $page_id,
                'site_id' => $site_id,
                'tpl_id' => $tpl_id
            ];
        //если блок последний, либо между
        }else{
            //получить блоки где sort = $request->sort .. n
            if ($sort)
            {
                $blocks = Block::getBlocksPageWhereSortFrom($page_id, $sort);
                foreach ($blocks as $block)
                {
                    $block->sort += 1;
                    $block->save();
                }
            }

            $data = [
                'sort' => $sort ?? $max+1,
                'page_id' => $page_id,
                'site_id' => $site_id,
                'tpl_id' => $tpl_id
            ];
        }

        return $data;
    }

}
?>