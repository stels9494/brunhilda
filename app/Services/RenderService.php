<?php
namespace App\Services;

use Illuminate\Support\Facades\Blade;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class RenderService
{

    /*рендеринг блока - посредством Blade*/
    public static function renderBlock($block)
    {
        // список системных переменных
        $vars = [
            'block_id' => $block->id,
        ];
        //список переменных шаблона
        foreach ($block->blockFields ?? [] as $field)
        {
            $vars[$field->name] = $field->{'var_'.$field->type};
        }
        //рендер
        $data = [
            'html' => htmlspecialchars_decode(self::render($block->tpl->html, $vars)),
            'css' => self::render($block->tpl->css, $vars),
            'js' => self::render($block->tpl->js, $vars),
        ];
        return $data;
    }



    public static function render($string, $data)
    {
        $php = Blade::compileString($string);
        $obLevel = ob_get_level();
        ob_start();
        extract($data, EXTR_SKIP);
        try {
            eval('?' . '>' . $php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw new FatalThrowableError($e);
        }

        return ob_get_clean();
    }

}

?>