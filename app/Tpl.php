<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Tpl extends Model implements HasMediaConversions
{
    use HasMediaTrait;


    protected $table = 'tpls';
    public $timestamps = false;
    protected $guarded = [];

    // protected $hidden = ['id', 'name'];
    protected $visible = ['id', 'name', 'img'];

    public function tplType()
    {
        return $this->belongsTo('App\TplType', 'type_id', 'id');
    }

    public function fields()
    {
        return $this->hasMany('App\Field');
    }

    public function blockFields()
    {
        return $this->hasMany('App\BlockField');
    }

    public function blocks()
    {
        return $this->hasMany('App\Block');
    }






    /*************РАБОТА С ИЗОБРАЖЕНИЯМИ***************/
    public function registerMediaConversions(Media $media = null)
    {
        //изображения для превью
        $this->addMediaConversion('preview')
              ->width(640)
              ->height(330)
              ->sharpen(10);
    }



    /***********************************CUSTOM FUNCTION******************************************/

    /*выполняет запрос на выборку конкретного шаблона с его филдами*/
    public static function getTpl($tpl_id)
    {
        return self::where('id', $tpl_id)
                    ->with('fields')
                    ->get()
                    ->first();
    }
}
