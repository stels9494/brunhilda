<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JsLib extends Model
{
    protected $table = 'js_libs';
    public $timestamps = false;
    protected $guarded = [];
}
