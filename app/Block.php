<?php

namespace App;

use App\BlockField;
use Illuminate\Database\Eloquent\Model;
use App\Events\BlockDeleting;

class Block extends Model
{
    protected $table = 'blocks';
    public $timestamps = false;
    protected $guarded = [];

    public function page()
    {
        return $this->belongsTo('App\Page');
    }

    public function tpl()
    {
        return $this->belongsTo('App\Tpl');
    }

    public function blockFields()
    {
        return $this->hasMany('App\BlockField', 'block_id', 'id');
    }

    public function data()
    {
        return $this->hasMany('App\Data', 'block_id', 'id');
    }

    public function site()
    {
        return $this->belongsTo('App\Site');
    }



    /********************************CUSTOM FUNCTION**********************************************/

    /*возвращает все блоки страницы, вместе с шаблонами и перемеными блоков.*/
    public static function getBlocksPage($page_id)
    {
        //запросить все блоки ()
        return self::where('page_id', $page_id)
                    ->with('tpl')
                    ->with('blockFields')
                    ->orderBy('sort')
                    ->get();
    }


    /*возвращает максимальное значение sort блока указанной страницы*/
    public static function getMaxSortOnPage($page_id)
    {
        return self::where('page_id', $page_id)
                    ->max('sort');
    }

    /*возвращает блоки где sort > $sort*/
    public static function getBlocksPageWhereSortFrom($page_id, $sort)
    {
        return self::where('page_id', $page_id)
                    ->where('sort', '>=', $sort)
                    ->orderBy('sort')
                    ->get();
    }

    public function copyFieldFromTpl($tpl_id)
    {
        $additionally = [
            'block_id' => $this->id,
            'page_id' => $this->page_id,
            'site_id' => $this->site_id
        ]; //дополнительные поля

        $tpl = Tpl::getTpl($tpl_id);
        foreach ($tpl->fields as $field)
        {
            $data = $field->toArray() + $additionally;
            BlockField::create($data);
        }
    }

    /*возвращает филд data для текущего блока*/
    public function getDataBlockField()
    {
        return BlockField::getDataBlockField($this->id);
    }

    /*получить все блоки*/
    public function getDataBlocks()
    {
        return Data::getDataBlock($this->id);
    }

    public function getBlockFields()
    {
        return BlockField::getBlockFields($this->id);
    }


    /*создает запись в Data с пустым (базовым) значением*/

    public function createDataDefault()
    {
        return Data::create([
            'full_name' => 'default',
            'tpl_id' => $this->tpl_id,
            'block_id' => $this->id,
            'mimetype' => 'default'
        ]);
    }




}
