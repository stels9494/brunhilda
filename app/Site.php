<?php

namespace App;

use App\Account;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\assignRole;

class Site extends Model
{
    protected $table = 'sites';
    protected $guarded = [];

    public function pages()
    {
        return $this->hasMany('App\Page');
    }

    // public function users()
    // {
    //     return $this->belongsToMany('App\User', 'site_user');
    // }

    public function blocks()
    {
        return $this->hasMany('App\Block');
    }

    public function accounts()
    {
        return $this->hasMany('App\Account');
    }










/******************************Работа с пользователями сайта*****************************************/

    /*прикрепляет пользователя к сайту с определенной ролью*/
    public function attachUser($user_id, $role)
    {
        //прикрепить пользователя к сайту
        $this->users()->attach($user_id);
        //проверить, что такая роль дейтсвительно есть

        //закрепить за пользователем роль
        User::find($user_id)->assignRole($role);
    }

    /*открепляет пользователя от сайта*/
    public function detachUser($user_id)
    {

    }

    /*Задает свой набор прав пользователя*/
    public function changeUserPermissions($user_id, $permissions)
    {

    }

    /*
        Создать аккаунт для указанного пользователя и текущего сайта
        @return Account
    */
    public function createAccount($user_id)
    {
        $user = User::find($user_id);
        return Account::create([
            'name' => $user->name,
            'email' => $user->name,
            'site_id' => $this->id,
            'user_id' => $user_id
        ]);
    }

}
