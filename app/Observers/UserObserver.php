<?php
namespace App\Observers;


use App\User;

class UserObserver
{
    public function created(User $user)
    {
        /*здесь нужно отправлять сообщение на почту*/

        /*начало временного кода*/
        $user->activate = true;
        $user->second_token = '';
        $user->password = bcrypt('123456');
        $user->save();

        /******до этого комментария временный код********/

        //пользовтаелю назначается роль
        if (request()->has('role'))
        {
            $user->syncRoles([request()->role]);
        }
    }

    public function updated(User $user)
    {
        //пользовтаелю назначается роль
        if (request()->has('role'))
        {
            $user->syncRoles([request()->role]);
        }
    }
}