<?php
namespace App\Observers;

use App\Block;


class BlockObserver
{
    public function deleting(Block $block)
    {
        //получить все Data для этого блока
        //удалить все для этого блока
        foreach ($block->data as &$datum)
        {
            $datum->delete();
        }
    }
}


?>