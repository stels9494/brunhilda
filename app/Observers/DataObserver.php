<?php
namespace App\Observers;

use App\Data;
use Illuminate\Support\Facades\Storage;



class DataObserver
{
    public function deleting(Data $data)
    {

        //Найти в blockfield data и вычеркнуть текущий id data
        $block_field = $data->block->getDataBlockField();
        $ar = json_decode($block_field->var_data);
        //простите меня за такой грубейший костыль, unset на массиве $ar в некоторых случая не срабатывает
        //начало костыля
            $new_ar = [];
            foreach ($ar as $key => $value)
            {
                if ($data->id == $value) continue;
                $new_ar[$key] = $value;
            }
            $ar = $new_ar;
            $block_field->var_data = json_encode($ar);
            $block_field->save();
        //конец костыля

        //получить full_name
        $name = $data->full_name;
        //получить папку размещения файла
        $folder = substr($name, 0, stripos($name, '_'));
        $path = 'public/data/'.$folder.'/'.$name;
        //удалить файл
        Storage::delete($path);
    }

}


?>