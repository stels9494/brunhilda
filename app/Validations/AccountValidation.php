<?php
namespace App\Validations;

use App\Account;
use App\Site;
use App\User;
use Illuminate\Validation\Rule;

class AccountValidation
{
    public static function store($request)
    {
        $site = Site::where('prefix', $request->domain)->first();
        $user = User::where('email', $request->email)->first();
        $data = $request->validate([
            'email' => 'required|string|email|exists:users,email',
            'domain' => 'required|string|exists:sites,prefix',
            'email' => Rule::unique('accounts')->where(function ($query) use ($site) {
                return $query->where('site_id', $site->id); //проверка уникальности аккаунта для этого сайта и пользователя
            }),
        ]) + [
            'user_id' => $user->id,
            'site_id' => $site->id,
            'name' => $user->name,
        ];
        unset($data['domain']);
        return $data;
    }

    public static function update($request)
    {
        $site = Site::where('prefix', $request->domain)->first();
        $user = User::where('email', $request->email)->first();
        $data = $request->validate([
            'email' => 'required|string|email|exists:users,email',
            'domain' => 'required|string|exists:sites,prefix',
            'email' => Rule::exists('accounts')->where(function ($query) use ($site) {
                return $query->where('site_id', $site->id);
            }),
        ]) + [
            'user_id' => $user->id,
            'site_id' => $site->id,
            'name' => $user->name,
        ];
        unset($data['domain']);
        return $data;
    }
}