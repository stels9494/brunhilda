<?php
namespace App\Validations;

class UserValidation
{
    public static function store($request)
    {
        return $request->validate([
            'email' => 'required|email|unique:users,email',
            'name' => 'required|string',
        ]) + [
            'second_token' => bcrypt($request->email),
            'activate' => false,
            'password' => bcrypt('123456'),
        ];
    }

    public static function update($request)
    {
        return $request->validate([
            'email' => 'required|email',
            'name' => 'required|string',
        ]);
    }
}