<?php
namespace App\Validations\Api;

use Illuminate\Support\Facades\Validator;


class PageValidation
{
    public static function createPage($request)
    {
        $data = Validator::make($request->all()+['site_id' => $request->site_id], [
            'prefix' => 'required|unique:pages,prefix|string',
            'title' => 'required|string',
            'site_id' => 'required|integer|exists:sites,id',
        ]);
        if ($data->fails()) return false;
        $data = $data->getData();
        unset($data['token'], $data['page']);
        return $data;
    }
}