<?php
namespace App\Validations;

class RoleValidation
{
    public static function store($request)
    {
        $data = $request->validate([
            'role' => 'required_without:permission|unique:roles,name',
            'permission' => 'required_without:role|unique:permissions,name',
        ]);
        return $data;
    }

    public static function update($request)
    {
        return $data;
    }
}