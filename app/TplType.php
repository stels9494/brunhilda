<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class TplType extends Model
{
    protected $table = 'tpl_types';
    public $timestamps = false;
    protected $guarded = [];



    /*********************************RELATIONSHIP********************************/
    public function tpls()
    {
        return $this->hasMany('App\Tpl', 'type_id', 'id');
    }





    /***********************************CUSTOM FUNCTIONS*****************************************/

    /*возвращает типы и шаблоны*/
    public static function getTypesWithTemplates()
    {
        $types = TplType::with('tpls')->get();
        foreach ($types as &$type)
        {
            $type['active'] = false;
            foreach ($type->tpls as &$tpl)
                $tpl['img'] = ($tpl->preview)? URL::to('/').$tpl->preview: URL::to('/')."/images/imageNotFound.jpg";
        }
        return $types;
    }



}
