<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Account extends Model
{
    use HasRoles;

    protected $table = 'accounts';
    public $timestamps = false;

    protected $guarded = [];



/*************************RELATIONSHIP***********************/
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function site()
    {
        return $this->belongsTo('App\Site');
    }


/************************FUNCTIONS**********************/


    /*изменяет или задает роль текущего аккаунта*/
    public function attachRole($role = null)
    {
        return $this->syncRoles([$role]);
    }

    /*задает права аккаунта, на случай, если нужен акканут с уникальным набором прав*/
    public function attachPermission($permissions = [])
    {
        return $this->syncPermisssions($permissions);
    }


}
