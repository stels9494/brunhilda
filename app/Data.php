<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Storage;
use App\Events\DataDeleting;

class Data extends Model
{
    protected $table = 'data';
    public $timestamps = true;
    protected $guarded = [];
    protected $visible = ['id', 'full_name', 'mimetype'];

    // protected $dispatchesEvents = [
    //     'deleting' => DataDeleting::class,
    // ];


    public function block()
    {
        return $this->belongsTo('App\Block');
    }



    /*добавление нового адреса файла в таблицу data*/
    public static function addData($name, $block_id, $tpl_id, $mimetype)
    {
        // echo URL::to('/').'/'.$path;
        return self::create([
            // 'full_name' => $path,
            'full_name' => $name,
            'block_id' => $block_id,
            'tpl_id' => $tpl_id,
            'mimetype' => $mimetype,
        ]);
    }

    /*все записи из таблицы data для блока*/
    public static function getDataBlock($block_id)
    {
        return self::where('block_id', $block_id)->get();
    }
    /*обновление текущей записи*/
    public function updateData($new_name, $mimetype)
    {
        return self::update([
            'full_name' => $new_name,
            'mimetype' => $mimetype,
        ]);
    }

}
