<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CssLib extends Model
{
    protected $table = 'css_libs';
    public $timestamps = false;
    protected $guarded = [];
}
