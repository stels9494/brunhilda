<?php

namespace App\Providers;


use App\Account;
use App\Block;
use App\Data;
use App\User;
use App\Observers\AccountObserver;
use App\Observers\BlockObserver;
use App\Observers\DataObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Data::observe(DataObserver::class);
        Block::observe(BlockObserver::class);
        User::observe(UserObserver::class);
        Account::observe(AccountObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
