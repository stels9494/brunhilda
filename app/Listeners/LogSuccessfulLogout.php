<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use JWTAuth;

class LogSuccessfulLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        JWTAuth::setToken($request->session()->get('jwt_token'));
        JWTAuth::invalidate();
        $request->session()->forget('jwt_token'); //но перед эти нужно онулировать токен и закрыть сессию в апи
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
    }
}
