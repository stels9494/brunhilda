<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockField extends Model
{
    protected $table = 'block_field';
    public $timestamps = false;
    protected $guarded = ['id'];

    public function block()
    {
        return $this->belongsTo('App\Block');
    }

    public function tpl()
    {
        return $this->belongsTo('App\Tpl');
    }

    public function page_id()
    {
        return $this->belongsTo('App\Page');
    }


    /*возвращает филд data указанного блока*/
    public static function getDataBlockField($block_id)
    {
        return self::where('block_id', $block_id)
                ->where('type', 'data')
                ->where('name', 'data')
                ->get()
                ->first();
    }


    /*возвращает все BlockField блока*/
    public static function getBlockFields($block_id)
    {
        return self::where('block_id', $block_id)
                    ->get();
    }
}
