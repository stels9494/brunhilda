-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: brunhilda
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `site_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_user_id_foreign` (`user_id`),
  KEY `accounts_site_id_foreign` (`site_id`),
  CONSTRAINT `accounts_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_field`
--

DROP TABLE IF EXISTS `block_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `var_char` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `var_int` int(11) DEFAULT NULL,
  `var_text` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `block_id` int(10) unsigned NOT NULL,
  `tpl_id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `site_id` int(10) unsigned NOT NULL,
  `var_data` longtext COLLATE utf8mb4_unicode_ci,
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `block_field_block_id_foreign` (`block_id`),
  KEY `block_field_tpl_id_foreign` (`tpl_id`),
  KEY `block_field_page_id_foreign` (`page_id`),
  CONSTRAINT `block_field_block_id_foreign` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `block_field_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `block_field_tpl_id_foreign` FOREIGN KEY (`tpl_id`) REFERENCES `tpls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_field`
--

LOCK TABLES `block_field` WRITE;
/*!40000 ALTER TABLE `block_field` DISABLE KEYS */;
INSERT INTO `block_field` VALUES (81,NULL,NULL,NULL,'data','data',38,1,10,1,'[]',0),(82,NULL,NULL,'Maureen Zehring','autor','text',38,1,10,1,NULL,1),(83,NULL,NULL,'A headline\'s purpose is to quickly and briefly draw attention to the story. It is generally written by a copy editor, but may also be written by the writer, the page layout designer, or other editors.','text','text',38,1,10,1,NULL,1),(84,NULL,NULL,'How to write headlines that actually work?','header','text',38,1,10,1,NULL,1),(85,NULL,NULL,NULL,'data','data',39,4,10,1,'[52]',0),(86,NULL,NULL,'ЧЕРЕПАШИЙ СУП','title','text',39,4,10,1,NULL,1),(87,NULL,NULL,NULL,'data','data',40,4,10,1,'[51]',0),(88,NULL,NULL,'ЧЕРЕПАШИЙ СУП','title','text',40,4,10,1,NULL,1),(89,NULL,NULL,NULL,'data','data',41,4,11,1,'[]',0),(90,NULL,NULL,'ЧЕРЕПАШИЙ СУП','title','text',41,4,11,1,NULL,1);
/*!40000 ALTER TABLE `block_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sort` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `site_id` int(10) unsigned NOT NULL,
  `tpl_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `blocks_page_id_foreign` (`page_id`),
  KEY `blocks_site_id_foreign` (`site_id`),
  KEY `blocks_tpl_id_foreign` (`tpl_id`),
  CONSTRAINT `blocks_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `blocks_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `blocks_tpl_id_foreign` FOREIGN KEY (`tpl_id`) REFERENCES `tpls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blocks`
--

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;
INSERT INTO `blocks` VALUES (38,1,10,1,1),(39,3,10,1,4),(40,2,10,1,4),(41,1,11,1,4);
/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `css_libs`
--

DROP TABLE IF EXISTS `css_libs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `css_libs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `css_libs`
--

LOCK TABLES `css_libs` WRITE;
/*!40000 ALTER TABLE `css_libs` DISABLE KEYS */;
INSERT INTO `css_libs` VALUES (1,'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css','slick');
/*!40000 ALTER TABLE `css_libs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data`
--

DROP TABLE IF EXISTS `data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `block_id` int(10) unsigned DEFAULT NULL,
  `tpl_id` int(10) unsigned NOT NULL,
  `mimetype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `data_block_id_foreign` (`block_id`),
  KEY `data_tpl_id_foreign` (`tpl_id`),
  CONSTRAINT `data_block_id_foreign` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `data_tpl_id_foreign` FOREIGN KEY (`tpl_id`) REFERENCES `tpls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data`
--

LOCK TABLES `data` WRITE;
/*!40000 ALTER TABLE `data` DISABLE KEYS */;
INSERT INTO `data` VALUES (51,'2018-02-19 06:25:28','2018-02-19 06:25:43','1_51_1519032343.jpg',40,4,'image/jpeg'),(52,'2018-02-19 06:26:27','2018-02-19 06:26:27','1_52_1519032387.jpg',39,4,'image/jpeg');
/*!40000 ALTER TABLE `data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `var_char` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `var_text` text COLLATE utf8mb4_unicode_ci,
  `var_int` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tpl_id` int(10) unsigned NOT NULL,
  `var_data` longtext COLLATE utf8mb4_unicode_ci,
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fields_tpl_id_foreign` (`tpl_id`),
  CONSTRAINT `fields_tpl_id_foreign` FOREIGN KEY (`tpl_id`) REFERENCES `tpls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
INSERT INTO `fields` VALUES (1,NULL,NULL,NULL,'data','data',1,'[]',0),(5,NULL,'Maureen Zehring',NULL,'text','autor',1,NULL,1),(6,NULL,'A headline\'s purpose is to quickly and briefly draw attention to the story. It is generally written by a copy editor, but may also be written by the writer, the page layout designer, or other editors.',NULL,'text','text',1,NULL,1),(7,NULL,'How to write headlines that actually work?',NULL,'text','header',1,NULL,1),(15,NULL,NULL,NULL,'data','data',4,'[]',0),(16,NULL,'ЧЕРЕПАШИЙ СУП',NULL,'text','title',4,NULL,1);
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `js_libs`
--

DROP TABLE IF EXISTS `js_libs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `js_libs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `js_libs`
--

LOCK TABLES `js_libs` WRITE;
/*!40000 ALTER TABLE `js_libs` DISABLE KEYS */;
INSERT INTO `js_libs` VALUES (1,'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js','jquery'),(2,'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js','slick');
/*!40000 ALTER TABLE `js_libs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_01_15_153138_create_permission_tables',1),(4,'2018_01_15_153141_create_tables_for_brunhilda',1),(5,'2018_01_16_080312_create_media_table',1),(6,'2018_02_01_110217_create_data_table',1),(7,'2018_02_07_144829_create_field_in_data_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_site_id_foreign` (`site_id`),
  CONSTRAINT `pages_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (10,'2018-02-09 06:38:23','2018-02-09 06:38:23','123','123',1),(11,'2018-02-19 06:12:05','2018-02-19 06:12:05','alex','alex',1);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_user`
--

DROP TABLE IF EXISTS `site_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_user` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_user`
--

LOCK TABLES `site_user` WRITE;
/*!40000 ALTER TABLE `site_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (1,'2018-02-05 05:56:58','2018-02-05 05:56:58','тестовый','test',0);
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tpl_types`
--

DROP TABLE IF EXISTS `tpl_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tpl_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tpl_types`
--

LOCK TABLES `tpl_types` WRITE;
/*!40000 ALTER TABLE `tpl_types` DISABLE KEYS */;
INSERT INTO `tpl_types` VALUES (1,'dfasdf'),(2,'Заголовки'),(3,'Галереи');
/*!40000 ALTER TABLE `tpl_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tpls`
--

DROP TABLE IF EXISTS `tpls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tpls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preview` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html` longtext COLLATE utf8mb4_unicode_ci,
  `css` longtext COLLATE utf8mb4_unicode_ci,
  `js` longtext COLLATE utf8mb4_unicode_ci,
  `js_libs` longtext COLLATE utf8mb4_unicode_ci,
  `css_libs` longtext COLLATE utf8mb4_unicode_ci,
  `type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tpls_type_id_foreign` (`type_id`),
  CONSTRAINT `tpls_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `tpl_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tpls`
--

LOCK TABLES `tpls` WRITE;
/*!40000 ALTER TABLE `tpls` DISABLE KEYS */;
INSERT INTO `tpls` VALUES (1,'Заголовок 1',NULL,'<div class=\"headline1 container\" data-block-id=\"{{ $block_id }}\">\r\n    <div class=\"headline1__uptitle\" data-field-name=\"autor\" data-field-type=\"text\">{{ $autor }}</div>\r\n    <div class=\"headline1__title\" data-field-name=\"header\" data-field-type=\"text\"><u>{{ $header }}</u></div>\r\n    <div class=\"headline1__text\" data-field-name=\"text\" data-field-type=\"text\">{{ $text }}</div>\r\n</div>','.headline1 {color: #000;font-family: Roboto, sans-serif;text-align: center;}.headline1__uptitle,.headline1__title {font-weight: 600;}.headline1__uptitle {letter-spacing: 2.5px;padding: 10px 0 90px;text-transform: uppercase;font-size: 14px;}.headline1__title {font-size: 80px;line-height: 1.1;}.headline1__text {font-weight: 300;font-size: 25px;padding: 50px 0 10px;}',NULL,'[]','[]',2),(4,'Слайдер',NULL,'<div data-block-id=\"{{ $block_id }}\">\r\n   <h1 data-field-type=\"text\" data-field-name=\"title\">{{ $title }}</h1>\r\n</div>\r\n\r\n<div class=\"banner-slider\" id=\"banner-slider-{{ $block_id }}\" data-block-id=\"{{ $block_id }}\">\r\n\r\n    <?php\r\n        $arId = json_decode($data[\'data\']);\r\n        if (!count($arId))\r\n        {\r\n            echo \'<img data-field-type=\"image\" src=\"\'.URL::to(\'/\').\"/images/imageNotFound.jpg\".\'\">\';\r\n        }\r\n        foreach ($arId as $id)\r\n        {\r\n            $name = \\App\\Data::find($id)->full_name;\r\n            $folder = substr($name, 0, stripos($name, \'_\'));\r\n            echo \'<img data-data-id=\"\'.$id.\'\" data-field-type=\"image\" src=\"\'.URL::to(\'/\').\'/storage/data/\'.$folder.\'/\'.$name.\'\">\';\r\n        }\r\n    ?>\r\n</div>','.banner-slider {\r\n  width: 1000px;\r\n  margin: 0 auto;\r\n}\r\n.banner-slider .slick-slide {\r\n  height: 500px;\r\n  object-fit: cover;\r\n}\r\n.banner-slider .slider-arrow {\r\n  top: calc(50% - 16.5px);\r\n}\r\n.banner-slider .slider-arrow._prev {\r\n  left: 20px;\r\n}\r\n.banner-slider .slider-arrow._next {\r\n  right: 20px;  \r\n}\r\n.banner-slider .slick-dots {\r\n  display: flex;\r\n  padding: 0;\r\n  margin: 0;\r\n  position: absolute;\r\n  bottom: 30px;\r\n  left: 0;\r\n  right: 0;\r\n  list-style: none;\r\n  justify-content: center;\r\n}\r\n.banner-slider .slick-dots .slick-active button {\r\n  background-color: #fff;\r\n}\r\n.banner-slider .slick-dots>li {\r\n    margin: 0 6px\r\n}\r\n.banner-slider .slick-dots li button {\r\n  font-size: 0;\r\n  line-height: 0;\r\n  display: block;\r\n  width: 10px;\r\n  height: 10px;\r\n  padding: 5px;\r\n  cursor: pointer;\r\n  color: transparent;\r\n  border: 0;\r\n  outline: none;\r\n  background: transparent;\r\n  border-radius: 50%;\r\n  border: 1px solid #fff;\r\n}\r\n\r\n.banner-slider .slider-arrow {\r\n    position: absolute;\r\n    height: 35px;\r\n    width: 35px;\r\n    border-radius: 50%;\r\n    border: 1px solid #c4c0c1;\r\n    background-color: #fff;\r\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAOCAYAAADjXQYbAAAAoElEQVQYlW3QoQ5BARQG4O/emaBIMlf3FmZmU3gAQVZEuibINs0UzSRF8Q6CFzDBFBvbHeXe7bJ72tl3tv/fCaIokjMNTMMcqGGP3T9WcMAC6yyWsMMWc0ixiA3OmKTXIQKskn2IT4oFzFBHE3G2QIgybnj91w4xwhPLJOIHYwxQTSJ+EN7ooY1xtlA6D7RwwhXrLEqKdXDEPe+3F3TR/wJfzxx9vgOAiAAAAABJRU5ErkJggg==);\r\n    background-position: 50%;\r\n    background-repeat: no-repeat;\r\n    z-index: 1;\r\n    padding: 0;\r\n    transition: all .3s;\r\n}\r\n\r\n.banner-slider .slider-arrow:hover {\r\n   background-color: #c4c0c1;\r\n}\r\n\r\n.banner-slider .slider-arrow._next {\r\n  transform: rotate(180deg);\r\n}','$(\'#banner-slider-{{ $block_id }}\').slick({\r\n  rows: 0,\r\n  fade: true,\r\n  prevArrow: \'<button type=\"button\" class=\"slider-arrow _prev\"></button>\',\r\n  nextArrow: \'<button type=\"button\" class=\"slider-arrow _next\"></button>\',\r\n  slidesToShow: 1,\r\n  autoplay: true,\r\n  dots: true\r\n});','[\"https:\\/\\/cdnjs.cloudflare.com\\/ajax\\/libs\\/jquery\\/3.2.1\\/jquery.js\",\"https:\\/\\/cdnjs.cloudflare.com\\/ajax\\/libs\\/slick-carousel\\/1.8.1\\/slick.js\"]','[\"https:\\/\\/cdnjs.cloudflare.com\\/ajax\\/libs\\/slick-carousel\\/1.8.1\\/slick.css\"]',3);
/*!40000 ALTER TABLE `tpls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `second_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activate` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-21 11:32:17
