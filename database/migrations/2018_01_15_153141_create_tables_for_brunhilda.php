<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesForBrunhilda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->string('prefix')->uniquie();
            $table->boolean('activate')->default(false);
        });

        Schema::create('site_user', function (Blueprint $table) {
            $table->timestamps();
            $table->integer('site_id')->unsigned();
            $table->integer('user_id')->unsigned();
        });
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title')->nullable();
            $table->string('prefix');
            $table->integer('site_id')->unsigned();
        });
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sort')->unsigned();
            $table->integer('page_id')->unsigned();
            $table->integer('site_id')->unsigned();
            $table->integer('tpl_id')->unsigned();
        });
        Schema::create('block_field', function (Blueprint $table) {
            $table->increments('id');
            $table->string('var_char')->nullable();
            $table->integer('var_int')->nullable();
            $table->text('var_text')->nullable();
            $table->string('name')->nullable();
            $table->string('type');
            $table->integer('block_id')->unsigned();
            $table->integer('tpl_id')->unsigned();
            $table->integer('page_id')->unsigned();
            $table->integer('site_id')->unsigned();
        });
        Schema::create('tpls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('preview')->nullable();
            $table->longText('html')->nullable();
            $table->longText('css')->nullable();
            $table->longText('js')->nullable();
            $table->longText('js_libs')->nullable();
            $table->longText('css_libs')->nullable();
            $table->integer('type_id')->unsigned();
        });
        Schema::create('fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('var_char')->nullable();
            $table->text('var_text')->nullable();
            $table->integer('var_int')->nullable();
            $table->string('type');
            $table->string('name')->nullable();
            $table->integer('tpl_id')->unsigned();
        });
        Schema::create('tpl_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
        });
        Schema::create('js_libs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->nullable();
            $table->string('name')->nullable();
        });
        Schema::create('css_libs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->nullable();
            $table->string('name')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('second_token');
            $table->boolean('activate');
        });
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('site_id')->unsigned();
            $table->string('name');
            $table->string('email')->uniquie();
        });





        /*********************************FOREIGN KEYS****************************************/

        Schema::table('pages', function (Blueprint $table) {
            $table->foreign('site_id')
                    ->references('id')
                    ->on('sites')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
        // Schema::table('site_user', function (Blueprint $table) {
        //     $table->foreign('site_id')
        //             ->references('id')
        //             ->on('sites')
        //             ->onDelete('cascade')
        //             ->onUpdate('cascade');
        //     $table->foreign('user_id')
        //             ->references('id')
        //             ->on('users')
        //             ->onDelete('cascade')
        //             ->onUpdate('cascade');
        // });
        Schema::table('blocks', function (Blueprint $table) {
            $table->foreign('page_id')
                    ->references('id')
                    ->on('pages')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('site_id')
                    ->references('id')
                    ->on('sites')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('tpl_id')
                    ->references('id')
                    ->on('tpls')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('block_field', function (Blueprint $table) {
            $table->foreign('block_id')
                    ->references('id')
                    ->on('blocks')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('tpl_id')
                    ->references('id')
                    ->on('tpls')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('page_id')
                    ->references('id')
                    ->on('pages')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('tpls', function (Blueprint $table) {
            $table->foreign('type_id')
                    ->references('id')
                    ->on('tpl_types')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('fields', function (Blueprint $table) {
            $table->foreign('tpl_id')
                    ->references('id')
                    ->on('tpls')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('accounts', function (Blueprint $table) {
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('site_id')
                    ->references('id')
                    ->on('sites')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });


        /***********************************END FOREIGN KEY*******************************************/

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropForeign('pages_site_id_foreign');
        });

        // Schema::table('site_user', function (Blueprint $table) {
        //     $table->dropForeign('site_user_site_id_foreign');
        //     $table->dropForeign('site_user_user_id_foreign');
        // });

        Schema::table('blocks', function (Blueprint $table) {
            $table->dropForeign('blocks_page_id_foreign');
            $table->dropForeign('blocks_site_id_foreign');
            $table->dropForeign('blocks_tpl_id_foreign');
        });
        Schema::table('block_field', function (Blueprint $table) {
            $table->dropForeign('block_field_block_id_foreign');
            $table->dropForeign('block_field_tpl_id_foreign');
            $table->dropForeign('block_field_page_id_foreign');
        });
        Schema::table('tpls', function (Blueprint $table) {
            $table->dropForeign('tpls_type_id_foreign');
        });
        Schema::table('fields', function (Blueprint $table) {
            $table->dropForeign('fields_tpl_id_foreign');
        });
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropForeign('accounts_user_id_foreign');
            $table->dropForeign('accounts_site_id_foreign');
        });

        Schema::dropIfExists('sites');
        Schema::dropIfExists('pages');
        Schema::dropIfExists('blocks');
        Schema::dropIfExists('block_field');
        Schema::dropIfExists('tpls');
        Schema::dropIfExists('fields');
        Schema::dropIfExists('tpl_types');
        Schema::dropIfExists('js_libs');
        Schema::dropIfExists('css_libs');


        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('second_token');
            $table->dropColumn('activate');
        });
    }
}
