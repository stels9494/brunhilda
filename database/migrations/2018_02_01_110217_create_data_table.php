<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('full_name');
            $table->integer('block_id')->unsigned()->nullable();
            $table->integer('tpl_id')->unsigned();
            /*FOREIGN KEYS*/
            $table->foreign('block_id')
                  ->references('id')
                  ->on('blocks')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('tpl_id')
                  ->references('id')
                  ->on('tpls')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });

        Schema::table('fields', function (Blueprint $table) {
            $table->longText('var_data')->nullable();
            $table->boolean('removable')->default(true);
        });

        Schema::table('block_field', function (Blueprint $table) {
            $table->longText('var_data')->nullable();
            $table->boolean('removable')->default(true);
        });

        /****************FOREIGN KEY*******************/


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('data');

        Schema::table('fields', function (Blueprint $table) {
            $table->dropColumn('var_data');
            $table->dropColumn('removable');
        });
        Schema::table('block_field', function (Blueprint $table) {
            $table->dropColumn('var_data');
            $table->dropColumn('removable');
        });
    }
}
